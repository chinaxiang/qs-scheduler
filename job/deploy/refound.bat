echo ---rootDisk----
set "rootDisk=C:"
echo ---project source dir----
set "parentDir=Users\yanxiang.huang\workspace\oschina\qs-scheduler"
echo ---project name----
set "project=job"

set "projectUpDir=%rootDisk%\%parentDir%"

call %CATALINA_HOME%\bin\shutdown.bat
call mvnbuild.bat

rmdir /s /q %CATALINA_HOME%\webapps\%project%
del %CATALINA_HOME%\webapps\%project%.war
copy %projectUpDir%\%project%\target\%project%.war %CATALINA_HOME%\webapps\

%rootDisk%
cd %CATALINA_HOME%\bin

call %CATALINA_HOME%\bin\startup.bat