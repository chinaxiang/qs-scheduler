/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.integration;

import com.quanshi.scheduler.job.facade.dto.Secret;

/**
 * 
 * @author yanxiang.huang 2017-07-27 14:56:07
 */
public interface DubboMonitor
{

    /**
     * 
     *
     * @param msg
     * @return
     */
    Secret tell( String msg );

}
