///**
// * QUANSHI.com Inc.
// * Copyright (c) 2016-2017 All Rights Reserved.
// */
//package com.quanshi.scheduler.job.integration.impl;
//
//import javax.annotation.Resource;
//
//import org.springframework.context.annotation.Lazy;
//import org.springframework.stereotype.Service;
//
//import com.quanshi.scheduler.job.facade.HelloDubboFacade;
//import com.quanshi.scheduler.job.facade.dto.Secret;
//import com.quanshi.scheduler.job.integration.BaseIntegration;
//import com.quanshi.scheduler.job.integration.DubboMonitor;
//
///**
// * 
// * @author yanxiang.huang 2017-07-27 14:56:32
// */
//@Service
//@Lazy(value = true)
//public class DubboMonitorImpl extends BaseIntegration implements DubboMonitor
//{
//
//    @Resource(name = "hiDubbo")
//    private HelloDubboFacade hiDubbo;
//    
//    @Override
//    public Secret tell(String msg) {
//        logger.info( "tell from dubbo monitor:{}.", msg );
//        return hiDubbo.tell( msg );
//    }
//}
