/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * 
 * @author yanxiang.huang 2017-07-27 14:54:21
 */
public class BaseIntegration implements InitializingBean
{
    
    protected Logger logger = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void afterPropertiesSet() throws Exception
    {
    }

}
