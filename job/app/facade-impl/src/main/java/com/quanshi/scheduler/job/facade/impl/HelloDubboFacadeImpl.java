/**
 * 
 */
package com.quanshi.scheduler.job.facade.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.quanshi.scheduler.job.facade.HelloDubboFacade;
import com.quanshi.scheduler.job.facade.dto.Secret;


/**
 * hello dubbo implement.
 * 
 * @author yanxiang.huang 2017-07-06 10:07:57
 */
public class HelloDubboFacadeImpl implements HelloDubboFacade, InitializingBean
{
    /** logger */
    private Logger logger = LoggerFactory.getLogger( this.getClass() );
    
    @Override
    public void hello()
    {
        Date today = new Date();
        logger.info( "hello dubbo, today is {}.", today);
    }

    @Override
    public Secret tell( String msg )
    {
        logger.info( "received consumer msg:{}.", msg );
        Secret s = new Secret();
        s.setMsg( "I received your secret msg:" + msg );
        return s;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        logger.info( "hello dubbo facade impl:{}.", this.hashCode() );
    }
}
