/**
 * 
 */
package com.quanshi.scheduler.job.facade.dto;

import java.io.Serializable;

/**
 * 
 * @author yanxiang.huang 2017-07-06 10:52:31
 */
public class Secret implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = -6645378622665889582L;

    private String msg;

    /**
     * @return the msg
     */
    public String getMsg()
    {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg( String msg )
    {
        this.msg = msg;
    }
    
    
}
