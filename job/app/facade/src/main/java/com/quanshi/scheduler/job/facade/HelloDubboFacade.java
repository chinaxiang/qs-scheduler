/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.facade;

import com.quanshi.scheduler.job.facade.dto.Secret;

/**
 * 
 * @author yanxiang.huang 2017-07-19 19:02:01
 */
public interface HelloDubboFacade
{

    /**
     * just say hello dubbo.
     *
     */
    void hello();
    
    /**
     * just tell consumer the secret msg.
     *
     */
    Secret tell(String msg);
    
}
