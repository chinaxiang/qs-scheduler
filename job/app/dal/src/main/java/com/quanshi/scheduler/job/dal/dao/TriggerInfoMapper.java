package com.quanshi.scheduler.job.dal.dao;

import com.quanshi.scheduler.job.dal.entity.TriggerInfo;
import com.quanshi.scheduler.job.dal.entity.TriggerInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TriggerInfoMapper {
    long countByExample(TriggerInfoExample example);

    int deleteByExample(TriggerInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TriggerInfo record);

    int insertSelective(TriggerInfo record);

    List<TriggerInfo> selectByExampleWithBLOBs(TriggerInfoExample example);

    List<TriggerInfo> selectByExample(TriggerInfoExample example);

    TriggerInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TriggerInfo record, @Param("example") TriggerInfoExample example);

    int updateByExampleWithBLOBs(@Param("record") TriggerInfo record, @Param("example") TriggerInfoExample example);

    int updateByExample(@Param("record") TriggerInfo record, @Param("example") TriggerInfoExample example);

    int updateByPrimaryKeySelective(TriggerInfo record);

    int updateByPrimaryKeyWithBLOBs(TriggerInfo record);

    int updateByPrimaryKey(TriggerInfo record);
}