package com.quanshi.scheduler.job.dal.dao;

import com.quanshi.scheduler.job.dal.entity.TriggerLog;
import com.quanshi.scheduler.job.dal.entity.TriggerLogExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface TriggerLogMapper {
    long countByExample(TriggerLogExample example);

    int deleteByExample(TriggerLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TriggerLog record);

    int insertSelective(TriggerLog record);

    List<TriggerLog> selectByExample(TriggerLogExample example);

    TriggerLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TriggerLog record, @Param("example") TriggerLogExample example);

    int updateByExample(@Param("record") TriggerLog record, @Param("example") TriggerLogExample example);

    int updateByPrimaryKeySelective(TriggerLog record);

    int updateByPrimaryKey(TriggerLog record);

    /**
     * 清理历史日志
     *
     * @param params
     */
    void clearLog( Map<String, Object> params );
}