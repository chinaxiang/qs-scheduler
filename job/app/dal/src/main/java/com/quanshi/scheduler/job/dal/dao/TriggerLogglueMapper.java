package com.quanshi.scheduler.job.dal.dao;

import com.quanshi.scheduler.job.dal.entity.TriggerLogglue;
import com.quanshi.scheduler.job.dal.entity.TriggerLogglueExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TriggerLogglueMapper {
    long countByExample(TriggerLogglueExample example);

    int deleteByExample(TriggerLogglueExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TriggerLogglue record);

    int insertSelective(TriggerLogglue record);

    List<TriggerLogglue> selectByExampleWithBLOBs(TriggerLogglueExample example);

    List<TriggerLogglue> selectByExample(TriggerLogglueExample example);

    TriggerLogglue selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TriggerLogglue record, @Param("example") TriggerLogglueExample example);

    int updateByExampleWithBLOBs(@Param("record") TriggerLogglue record, @Param("example") TriggerLogglueExample example);

    int updateByExample(@Param("record") TriggerLogglue record, @Param("example") TriggerLogglueExample example);

    int updateByPrimaryKeySelective(TriggerLogglue record);

    int updateByPrimaryKeyWithBLOBs(TriggerLogglue record);

    int updateByPrimaryKey(TriggerLogglue record);
}