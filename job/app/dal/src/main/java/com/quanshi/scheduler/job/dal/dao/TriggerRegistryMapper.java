package com.quanshi.scheduler.job.dal.dao;

import com.quanshi.scheduler.job.dal.entity.TriggerRegistry;
import com.quanshi.scheduler.job.dal.entity.TriggerRegistryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TriggerRegistryMapper {
    long countByExample(TriggerRegistryExample example);

    int deleteByExample(TriggerRegistryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TriggerRegistry record);

    int insertSelective(TriggerRegistry record);

    List<TriggerRegistry> selectByExample(TriggerRegistryExample example);

    TriggerRegistry selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TriggerRegistry record, @Param("example") TriggerRegistryExample example);

    int updateByExample(@Param("record") TriggerRegistry record, @Param("example") TriggerRegistryExample example);

    int updateByPrimaryKeySelective(TriggerRegistry record);

    int updateByPrimaryKey(TriggerRegistry record);
}