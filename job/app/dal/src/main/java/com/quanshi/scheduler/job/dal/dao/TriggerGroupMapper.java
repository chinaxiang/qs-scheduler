package com.quanshi.scheduler.job.dal.dao;

import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.dal.entity.TriggerGroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TriggerGroupMapper {
    long countByExample(TriggerGroupExample example);

    int deleteByExample(TriggerGroupExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TriggerGroup record);

    int insertSelective(TriggerGroup record);

    List<TriggerGroup> selectByExample(TriggerGroupExample example);

    TriggerGroup selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TriggerGroup record, @Param("example") TriggerGroupExample example);

    int updateByExample(@Param("record") TriggerGroup record, @Param("example") TriggerGroupExample example);

    int updateByPrimaryKeySelective(TriggerGroup record);

    int updateByPrimaryKey(TriggerGroup record);
}