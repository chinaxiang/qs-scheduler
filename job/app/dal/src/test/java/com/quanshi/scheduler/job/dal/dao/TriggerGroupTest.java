/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.dal.dao;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quanshi.scheduler.job.dal.DalTest;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.dal.entity.TriggerGroupExample;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月8日 上午9:19:34
 */
public class TriggerGroupTest extends DalTest {

    @Resource
    private TriggerGroupMapper mapper;

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        logger.info("trigger group test init. mapper:{}.", mapper);
    }

    @Test
    public void selectTest() {
        TriggerGroupExample example = new TriggerGroupExample();
        PageHelper.startPage(1, 3);
        List<TriggerGroup> datas = mapper.selectByExample(example);
        for (TriggerGroup data : datas) {
            logger.info(data.getAppName());
        }
        PageInfo<TriggerGroup> page = new PageInfo<>(datas);
        logger.info("page info. total:{}, totalPage:{}, pageNo:{}, pageSize:{}.", page.getTotal(), page.getPages(), page.getPageNum(), page.getPageSize());
    }

}
