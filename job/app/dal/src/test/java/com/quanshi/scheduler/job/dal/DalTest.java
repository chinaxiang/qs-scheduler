/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.dal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Dal Test Base 
 * 
 * @author chinaxiang
 * @version 2017年7月8日 上午9:15:02
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dal-beans.xml"})
public class DalTest implements InitializingBean {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("dal test init.");
    }
    
    @Test
    public void justTest() {
        logger.info("just test.");
    }

}
