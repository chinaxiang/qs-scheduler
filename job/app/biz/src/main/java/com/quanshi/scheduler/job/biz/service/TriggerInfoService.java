/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.job.biz.model.req.TriggerInfoReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerInfoVO;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;

/**
 * 任务详情表处理逻辑
 * 
 * @author yanxiang.huang 2017-07-10 10:23:49
 */
public interface TriggerInfoService {

    /**
     * 根据ID获取任务
     *
     * @param id
     * @return
     */
    TriggerInfo get(long id);

    /**
     * 新增任务入库
     *
     * @param jobInfo
     * @return
     */
    boolean save(TriggerInfo jobInfo);

    /**
     * 删除任务
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 统计所有任务数
     *
     * @return
     */
    long findAllCount();

    /**
     * 获取任务分页
     *
     * @param req
     * @return
     */
    Ret<PageInfo<TriggerInfoVO>> findPage(TriggerInfoReq req);

    /**
     * 获取分页数据
     *
     * @param req
     * @return
     */
    Map<String, Object> findPageList(TriggerInfoReq req);

    /**
     * 更新任务信息
     *
     * @param jobInfo
     * @return
     */
    boolean update(TriggerInfo jobInfo);

    /**
     * 更新任务信息
     *
     * @param jobInfo
     * @return
     */
    boolean updateGlue(TriggerInfo jobInfo);

    /**
     * 验证分组是否存在任务
     *
     * @param groupId
     * @return
     */
    boolean checkHasUse(long groupId);

    /**
     * 根据分组获取任务列表
     *
     * @param jobGroup
     * @return
     */
    List<TriggerInfoVO> getByGroup(Long jobGroup);

    /**
     * 检查子任务，并触发子任务
     * 
     * @param id
     * @return
     */
    String triggerChild(Long id);

}
