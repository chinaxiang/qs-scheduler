/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.quanshi.scheduler.job.biz.service.BaseService;
import com.quanshi.scheduler.job.biz.service.TriggerRegistryService;
import com.quanshi.scheduler.job.dal.dao.TriggerRegistryMapper;
import com.quanshi.scheduler.job.dal.entity.TriggerRegistry;
import com.quanshi.scheduler.job.dal.entity.TriggerRegistryExample;

/**
 * 
 * @author yanxiang.huang 2017-07-11 10:45:04
 */
@Service
public class TriggerRegistryServiceImpl extends BaseService implements TriggerRegistryService
{
    
    @Resource
    private TriggerRegistryMapper mapper;

    @Override
    public List<TriggerRegistry> findAll( int deadTimeout )
    {
        Date deadTime = getDeadTime( deadTimeout );
        TriggerRegistryExample query = new TriggerRegistryExample();
        query.createCriteria().andUpdateTimeGreaterThan( deadTime );
        return mapper.selectByExample( query );
    }

    @Override
    public void removeDead( int deadTimeout )
    {
        logger.debug( "delete dead registry." );
        Date deadTime = getDeadTime( deadTimeout );
        TriggerRegistryExample query = new TriggerRegistryExample();
        query.createCriteria().andUpdateTimeLessThan( deadTime );
        mapper.deleteByExample( query );
    }
    
    @Override
    public boolean update( String registGroup, String registryKey, String registryValue )
    {
        TriggerRegistryExample query = new TriggerRegistryExample();
        query.createCriteria().andRegistryGroupEqualTo( registGroup ).andRegistryKeyEqualTo( registryKey ).andRegistryValueEqualTo( registryValue );
        List<TriggerRegistry> list = mapper.selectByExample( query );
        if (CollectionUtils.isNotEmpty( list )) {
            if (list.size() > 1) {
                logger.warn( "multiple registry. group:{}, key:{}, value:{}.", registGroup, registryKey, registryValue );
            }
            TriggerRegistry registry = list.get( 0 );
            registry.setUpdateTime( new Date() );
            int res = mapper.updateByPrimaryKey( registry );
            if (res == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean save( String registGroup, String registryKey, String registryValue )
    {
        TriggerRegistry registry = new TriggerRegistry();
        registry.setRegistryGroup( registGroup );
        registry.setRegistryKey( registryKey );
        registry.setRegistryValue( registryValue );
        registry.setUpdateTime( new Date() );
        int res = mapper.insert( registry );
        if (res == 1) {
            return true;
        }
        return false;
    }

    /**
     * 获取过期时间
     *
     * @param deadTimeout
     * @return
     */
    private Date getDeadTime(int deadTimeout) {
        Date deadTime = new Date();
        long time = deadTime.getTime() - deadTimeout * 1000;
        deadTime.setTime( time );
        return deadTime;
    }

}
