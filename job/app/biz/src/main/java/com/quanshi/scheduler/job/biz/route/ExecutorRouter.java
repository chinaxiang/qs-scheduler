/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.route;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.ExecutorBiz;
import com.quanshi.scheduler.core.biz.model.TriggerParam;
import com.quanshi.scheduler.core.rpc.netcom.jetty.NetComClientProxy;
import com.quanshi.scheduler.job.dal.entity.TriggerLog;

/**
 * 执行器路由
 * 
 * @author yanxiang.huang 2017-07-10 11:24:23
 */
public abstract class ExecutorRouter
{
    protected static Logger logger = LoggerFactory.getLogger( ExecutorRouter.class );
    
    public abstract String route(long jobId, ArrayList<String> addressList);

    public abstract Ret<String> routeRun( TriggerParam triggerParam, ArrayList<String> addressList, TriggerLog jobLog );

    protected static Ret<String> runExecutor( TriggerParam triggerParam, String address )
    {
        Ret<String> runResult = null;
        try
        {
            ExecutorBiz executorBiz = ( ExecutorBiz ) new NetComClientProxy( ExecutorBiz.class, address ).getObject();
            runResult = executorBiz.run( triggerParam );
        }
        catch ( Exception e )
        {
            logger.error( "run executor error.", e );
            runResult = new Ret<String>( Ret.FAIL_CODE, e.getMessage() );
        }

        StringBuffer runResultSB = new StringBuffer( "触发调度：" );
        runResultSB.append( "<br>address：" ).append( address );
        runResultSB.append( "<br>code：" ).append( runResult.getCode() );
        runResultSB.append( "<br>msg：" ).append( runResult.getMsg() );

        runResult.setMsg( runResultSB.toString() );
        return runResult;
    }
}
