/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.enums;

import com.quanshi.scheduler.job.biz.route.ExecutorRouter;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteBusyover;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteConsistentHash;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteFailover;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteFirst;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteLFU;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteLRU;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteLast;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteRandom;
import com.quanshi.scheduler.job.biz.route.strategy.ExecutorRouteRound;

/**
 * 执行器路由策略
 * 
 * @author yanxiang.huang 2017-07-10 11:18:58
 */
public enum ExecutorRouteStrategyEnum
{
    FIRST("第一个", new ExecutorRouteFirst()),
    LAST("最后一个", new ExecutorRouteLast()),
    ROUND("轮询", new ExecutorRouteRound()),
    RANDOM("随机", new ExecutorRouteRandom()),
    CONSISTENT_HASH("一致性HASH", new ExecutorRouteConsistentHash()),
    LEAST_FREQUENTLY_USED("最不经常使用", new ExecutorRouteLFU()),
    LEAST_RECENTLY_USED("最近最久未使用", new ExecutorRouteLRU()),
    FAILOVER("故障转移", new ExecutorRouteFailover()),
    BUSYOVER("忙碌转移", new ExecutorRouteBusyover());

    private ExecutorRouteStrategyEnum(String title, ExecutorRouter router) {
        this.title = title;
        this.router = router;
    }

    private String title;
    private ExecutorRouter router;

    public String getTitle() {
        return title;
    }
    public ExecutorRouter getRouter() {
        return router;
    }

    public static ExecutorRouteStrategyEnum match(String name, ExecutorRouteStrategyEnum defaultItem){
        if (name != null) {
            for (ExecutorRouteStrategyEnum item: ExecutorRouteStrategyEnum.values()) {
                if (item.name().equals(name)) {
                    return item;
                }
            }
        }
        return defaultItem;
    }

}
