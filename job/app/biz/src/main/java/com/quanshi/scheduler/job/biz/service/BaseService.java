/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * 
 * @author yanxiang.huang 2017-07-10 10:17:26
 */
public class BaseService implements InitializingBean
{
    
    protected Logger logger = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void afterPropertiesSet() throws Exception
    {
    }

}
