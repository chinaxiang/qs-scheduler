/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.List;

import com.quanshi.scheduler.job.dal.entity.TriggerRegistry;

/**
 * 任务注册到分组表处理逻辑
 * 
 * @author yanxiang.huang 2017-07-10 10:25:17
 */
public interface TriggerRegistryService
{

    /**
     * 获取所有可用
     *
     * @param deadTimeout
     * @return
     */
    List<TriggerRegistry> findAll( int deadTimeout );

    /**
     * 移除过期
     *
     * @param deadTimeout
     */
    void removeDead( int deadTimeout );

    /**
     * 更新
     *
     * @param registGroup
     * @param registryKey
     * @param registryValue
     * @return
     */
    boolean update( String registGroup, String registryKey, String registryValue );

    /**
     * 新增
     *
     * @param registGroup
     * @param registryKey
     * @param registryValue
     */
    boolean save( String registGroup, String registryKey, String registryValue );
    
    

}
