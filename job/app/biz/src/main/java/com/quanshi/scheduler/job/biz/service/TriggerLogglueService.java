/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.List;

import com.quanshi.scheduler.job.dal.entity.TriggerLogglue;

/**
 * glue版本控制表处理逻辑
 * 
 * @author yanxiang.huang 2017-07-10 10:26:30
 */
public interface TriggerLogglueService
{

    /**
     * 通过jobid删除任务glue日志
     *
     * @param jobId
     */
    void deleteByJobId( long jobId );

    /**
     * 通过jobid获取历史记录
     *
     * @param jobId
     * @return
     */
    List<TriggerLogglue> findByJobId( long jobId );

    /**
     * 保存记录
     *
     * @param jobId
     * @param glueType
     * @param glueRemark
     * @param glueSource
     */
    void saveLog( long jobId, String glueType, String glueRemark, String glueSource );

}
