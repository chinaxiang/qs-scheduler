/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.model.req;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author yanxiang.huang 2017-07-13 15:07:27
 */
public class TriggerInfoReq implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = -3774207237099034180L;

    private Long jobGroup;

    private String author;

    private String executorHandler;

    private String executorRouteStrategy;

    private String executorBlockStrategy;

    private String executorFailStrategy;

    private String glueType;

    private Date updateTimeStart;

    private Date updateTimeEnd;

    private int pageNo = 1;

    private int pageSize = 10;
    
    private int start = 0;
    
    private int length = 10;

    /**
     * @return the jobGroup
     */
    public Long getJobGroup()
    {
        return jobGroup;
    }

    /**
     * @param jobGroup the jobGroup to set
     */
    public void setJobGroup( Long jobGroup )
    {
        this.jobGroup = jobGroup;
    }

    /**
     * @return the author
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor( String author )
    {
        this.author = author;
    }

    /**
     * @return the executorHandler
     */
    public String getExecutorHandler()
    {
        return executorHandler;
    }

    /**
     * @param executorHandler the executorHandler to set
     */
    public void setExecutorHandler( String executorHandler )
    {
        this.executorHandler = executorHandler;
    }

    /**
     * @return the executorRouteStrategy
     */
    public String getExecutorRouteStrategy()
    {
        return executorRouteStrategy;
    }

    /**
     * @param executorRouteStrategy the executorRouteStrategy to set
     */
    public void setExecutorRouteStrategy( String executorRouteStrategy )
    {
        this.executorRouteStrategy = executorRouteStrategy;
    }

    /**
     * @return the executorBlockStrategy
     */
    public String getExecutorBlockStrategy()
    {
        return executorBlockStrategy;
    }

    /**
     * @param executorBlockStrategy the executorBlockStrategy to set
     */
    public void setExecutorBlockStrategy( String executorBlockStrategy )
    {
        this.executorBlockStrategy = executorBlockStrategy;
    }

    /**
     * @return the executorFailStrategy
     */
    public String getExecutorFailStrategy()
    {
        return executorFailStrategy;
    }

    /**
     * @param executorFailStrategy the executorFailStrategy to set
     */
    public void setExecutorFailStrategy( String executorFailStrategy )
    {
        this.executorFailStrategy = executorFailStrategy;
    }

    /**
     * @return the glueType
     */
    public String getGlueType()
    {
        return glueType;
    }

    /**
     * @param glueType the glueType to set
     */
    public void setGlueType( String glueType )
    {
        this.glueType = glueType;
    }

    /**
     * @return the updateTimeStart
     */
    public Date getUpdateTimeStart()
    {
        return updateTimeStart;
    }

    /**
     * @param updateTimeStart the updateTimeStart to set
     */
    public void setUpdateTimeStart( Date updateTimeStart )
    {
        this.updateTimeStart = updateTimeStart;
    }

    /**
     * @return the updateTimeEnd
     */
    public Date getUpdateTimeEnd()
    {
        return updateTimeEnd;
    }

    /**
     * @param updateTimeEnd the updateTimeEnd to set
     */
    public void setUpdateTimeEnd( Date updateTimeEnd )
    {
        this.updateTimeEnd = updateTimeEnd;
    }

    /**
     * @return the pageNo
     */
    public int getPageNo()
    {
        return pageNo;
    }

    /**
     * @param pageNo the pageNo to set
     */
    public void setPageNo( int pageNo )
    {
        this.pageNo = pageNo;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize()
    {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize( int pageSize )
    {
        this.pageSize = pageSize;
    }

    /**
     * @return the start
     */
    public int getStart()
    {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart( int start )
    {
        this.start = start;
    }

    /**
     * @return the length
     */
    public int getLength()
    {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength( int length )
    {
        this.length = length;
    }

}
