/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.route.strategy;

import java.util.ArrayList;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.model.TriggerParam;
import com.quanshi.scheduler.job.biz.route.ExecutorRouter;
import com.quanshi.scheduler.job.dal.entity.TriggerLog;

/**
 * 最后一个
 * 
 * @author yanxiang.huang 2017-07-10 13:39:50
 */
public class ExecutorRouteLast extends ExecutorRouter
{

    @Override
    public String route( long jobId, ArrayList<String> addressList )
    {
        return addressList.get(addressList.size() - 1);
    }

    @Override
    public Ret<String> routeRun( TriggerParam triggerParam, ArrayList<String> addressList, TriggerLog jobLog )
    {
        // address
        String address = route(triggerParam.getJobId(), addressList);
        jobLog.setExecutorAddress(address);

        // run executor
        Ret<String> runResult = runExecutor(triggerParam, address);
        runResult.setMsg("<br>----------------------<br>" + runResult.getMsg());

        return runResult;
    }

}
