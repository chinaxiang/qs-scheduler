/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.Map;

import com.quanshi.scheduler.job.dal.entity.TriggerInfo;

/**
 * 任务调度管理
 * 
 * @author chinaxiang
 * @version 2017年7月8日 上午9:39:04
 */
public interface JobService {
    
    /**
     * 新增任务
     *
     * @param jobInfo
     * @return
     */
    public boolean add(TriggerInfo jobInfo);
    
    /**
     * 重新部署任务（修改任务）
     *
     * @param jobInfo
     * @return
     */
    public boolean reschedule(TriggerInfo jobInfo);
    
    /**
     * 删除
     *
     * @param id
     * @return
     */
    public boolean remove(long id);
    
    /**
     * 暂停
     *
     * @param id
     * @return
     */
    public boolean pause(long id);
    
    /**
     * 恢复
     *
     * @param id
     * @return
     */
    public boolean resume(long id);
    
    /**
     * 触发任务（手动触发一次）
     *
     * @param id
     * @return
     */
    public boolean triggerJob(long id);

    /**
     * 控制台概览
     *
     * @return
     */
    public Map<String, Object> dashboard();

}
