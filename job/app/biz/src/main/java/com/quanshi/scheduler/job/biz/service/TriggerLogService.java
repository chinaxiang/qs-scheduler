/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.Date;
import java.util.Map;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.model.LogResult;
import com.quanshi.scheduler.job.biz.model.req.TriggerLogReq;
import com.quanshi.scheduler.job.dal.entity.TriggerLog;

/**
 * 任务日志表处理逻辑
 * 
 * @author yanxiang.huang 2017-07-10 10:24:27
 */
public interface TriggerLogService
{

    /**
     * 保存任务日志
     *
     * @param jobLog
     */
    void save( TriggerLog jobLog );

    /**
     * 更新任务日志
     *
     * @param jobLog
     */
    void update( TriggerLog jobLog );

    /**
     * 获取日志记录
     *
     * @param jobLogId
     * @return
     */
    TriggerLog get( Long jobLogId );

    /**
     * 统计所有任务日志数
     *
     * @return
     */
    long findAllCount();

    /**
     * 统计正在调度的任务数
     *
     * @return
     */
    long findTrggingCount();

    /**
     * 统计所有正在运行的日志数
     *
     * @return
     */
    long findRunningCount();

    /**
     * 统计调度成功的任务数
     *
     * @return
     */
    long findTriggerSucCount();

    /**
     * 统计调度失败的任务数
     *
     * @return
     */
    long findTriggerFailCount();

    /**
     * 统计执行成功的任务数
     *
     * @return
     */
    long findHandleSucCount();

    /**
     * 统计执行失败的任务数
     *
     * @return
     */
    long findHandleFailCount();

    /**
     * 通过jobid删除任务执行日志
     *
     * @param jobId
     */
    void deleteByJobId( long jobId );

    /**
     * 返回datatable请求数据
     *
     * @param req
     * @return
     */
    Map<String, Object> findDataPage( TriggerLogReq req );

    /**
     * 获取日志输出
     *
     * @param executorAddress
     * @param triggerTime
     * @param logId
     * @param fromLineNum
     * @return
     */
    Ret<LogResult> catLog( String executorAddress, long triggerTime, long logId, int fromLineNum );

    /**
     * 中止任务
     *
     * @param log
     * @return
     */
    Ret<?> killLog( TriggerLog log );

    /**
     * 清理历史日志
     *
     * @param jobGroup
     * @param jobId
     * @param clearBeforeTime
     * @param clearBeforeNum
     */
    void clearLog( Long jobGroup, Long jobId, Date clearBeforeTime, int clearBeforeNum );

}
