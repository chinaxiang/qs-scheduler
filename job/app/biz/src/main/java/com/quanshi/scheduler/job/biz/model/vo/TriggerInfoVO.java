/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.model.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author yanxiang.huang 2017-07-13 15:06:54
 */
public class TriggerInfoVO implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = -5821528991686974880L;

    private Long id;

    private Long jobGroup;

    private String jobCron;

    private String jobDesc;

    private Date updateTime;

    private String author;

    private String executorRouteStrategy;

    private String executorHandler;

    private String executorParam;

    private String executorBlockStrategy;

    private String executorFailStrategy;

    private String glueType;

    private String childJobkey;
    
    private String jobStatus;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    /**
     * @return the jobGroup
     */
    public Long getJobGroup()
    {
        return jobGroup;
    }

    /**
     * @param jobGroup the jobGroup to set
     */
    public void setJobGroup( Long jobGroup )
    {
        this.jobGroup = jobGroup;
    }

    /**
     * @return the jobCron
     */
    public String getJobCron()
    {
        return jobCron;
    }

    /**
     * @param jobCron the jobCron to set
     */
    public void setJobCron( String jobCron )
    {
        this.jobCron = jobCron;
    }

    /**
     * @return the jobDesc
     */
    public String getJobDesc()
    {
        return jobDesc;
    }

    /**
     * @param jobDesc the jobDesc to set
     */
    public void setJobDesc( String jobDesc )
    {
        this.jobDesc = jobDesc;
    }

    /**
     * @return the updateTime
     */
    public Date getUpdateTime()
    {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime( Date updateTime )
    {
        this.updateTime = updateTime;
    }

    /**
     * @return the author
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor( String author )
    {
        this.author = author;
    }

    /**
     * @return the executorRouteStrategy
     */
    public String getExecutorRouteStrategy()
    {
        return executorRouteStrategy;
    }

    /**
     * @param executorRouteStrategy the executorRouteStrategy to set
     */
    public void setExecutorRouteStrategy( String executorRouteStrategy )
    {
        this.executorRouteStrategy = executorRouteStrategy;
    }

    /**
     * @return the executorHandler
     */
    public String getExecutorHandler()
    {
        return executorHandler;
    }

    /**
     * @param executorHandler the executorHandler to set
     */
    public void setExecutorHandler( String executorHandler )
    {
        this.executorHandler = executorHandler;
    }

    /**
     * @return the executorParam
     */
    public String getExecutorParam()
    {
        return executorParam;
    }

    /**
     * @param executorParam the executorParam to set
     */
    public void setExecutorParam( String executorParam )
    {
        this.executorParam = executorParam;
    }

    /**
     * @return the executorBlockStrategy
     */
    public String getExecutorBlockStrategy()
    {
        return executorBlockStrategy;
    }

    /**
     * @param executorBlockStrategy the executorBlockStrategy to set
     */
    public void setExecutorBlockStrategy( String executorBlockStrategy )
    {
        this.executorBlockStrategy = executorBlockStrategy;
    }

    /**
     * @return the executorFailStrategy
     */
    public String getExecutorFailStrategy()
    {
        return executorFailStrategy;
    }

    /**
     * @param executorFailStrategy the executorFailStrategy to set
     */
    public void setExecutorFailStrategy( String executorFailStrategy )
    {
        this.executorFailStrategy = executorFailStrategy;
    }

    /**
     * @return the glueType
     */
    public String getGlueType()
    {
        return glueType;
    }

    /**
     * @param glueType the glueType to set
     */
    public void setGlueType( String glueType )
    {
        this.glueType = glueType;
    }

    /**
     * @return the childJobkey
     */
    public String getChildJobkey()
    {
        return childJobkey;
    }

    /**
     * @param childJobkey the childJobkey to set
     */
    public void setChildJobkey( String childJobkey )
    {
        this.childJobkey = childJobkey;
    }

    /**
     * @return the jobStatus
     */
    public String getJobStatus()
    {
        return jobStatus;
    }

    /**
     * @param jobStatus the jobStatus to set
     */
    public void setJobStatus( String jobStatus )
    {
        this.jobStatus = jobStatus;
    }

}
