/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.model.req;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author yanxiang.huang 2017-07-17 11:04:59
 */
public class TriggerLogReq implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = 6109898223531684653L;

    private Long jobGroup;

    private Long jobId;

    private Date dateStart;

    private Date dateEnd;

    private int start = 0;

    private int length = 10;

    /**
     * @return the jobGroup
     */
    public Long getJobGroup()
    {
        return jobGroup;
    }

    /**
     * @param jobGroup the jobGroup to set
     */
    public void setJobGroup( Long jobGroup )
    {
        this.jobGroup = jobGroup;
    }

    /**
     * @return the jobId
     */
    public Long getJobId()
    {
        return jobId;
    }

    /**
     * @param jobId the jobId to set
     */
    public void setJobId( Long jobId )
    {
        this.jobId = jobId;
    }

    /**
     * @return the dateStart
     */
    public Date getDateStart()
    {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart( Date dateStart )
    {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public Date getDateEnd()
    {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd( Date dateEnd )
    {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the start
     */
    public int getStart()
    {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart( int start )
    {
        this.start = start;
    }

    /**
     * @return the length
     */
    public int getLength()
    {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength( int length )
    {
        this.length = length;
    }

}
