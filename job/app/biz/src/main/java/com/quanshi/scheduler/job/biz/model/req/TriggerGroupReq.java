/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.model.req;

import java.io.Serializable;

/**
 * 
 * @author yanxiang.huang 2017-07-14 15:36:04
 */
public class TriggerGroupReq implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = -8089023365145679478L;

    private Long id;

    private String appName;

    private String title;

    private Byte order;

    private Byte addressType;

    private String addressList;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    /**
     * @return the appName
     */
    public String getAppName()
    {
        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public void setAppName( String appName )
    {
        this.appName = appName;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle( String title )
    {
        this.title = title;
    }

    /**
     * @return the order
     */
    public Byte getOrder()
    {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder( Byte order )
    {
        this.order = order;
    }

    /**
     * @return the addressType
     */
    public Byte getAddressType()
    {
        return addressType;
    }

    /**
     * @param addressType the addressType to set
     */
    public void setAddressType( Byte addressType )
    {
        this.addressType = addressType;
    }

    /**
     * @return the addressList
     */
    public String getAddressList()
    {
        return addressList;
    }

    /**
     * @param addressList the addressList to set
     */
    public void setAddressList( String addressList )
    {
        this.addressList = addressList;
    }

}
