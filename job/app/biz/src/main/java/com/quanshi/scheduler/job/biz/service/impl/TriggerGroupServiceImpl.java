/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.quanshi.scheduler.core.base.RegistryConfig;
import com.quanshi.scheduler.job.biz.model.req.TriggerGroupReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerGroupVO;
import com.quanshi.scheduler.job.biz.quartz.monitor.JobRegistryMonitor;
import com.quanshi.scheduler.job.biz.service.BaseService;
import com.quanshi.scheduler.job.biz.service.TriggerGroupService;
import com.quanshi.scheduler.job.biz.utils.BeanMapper;
import com.quanshi.scheduler.job.dal.dao.TriggerGroupMapper;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.dal.entity.TriggerGroupExample;

/**
 * 
 * @author yanxiang.huang 2017-07-11 10:27:27
 */
@Service
public class TriggerGroupServiceImpl extends BaseService implements TriggerGroupService
{

    @Resource
    private TriggerGroupMapper mapper;

    @Override
    public TriggerGroup get( Long id )
    {
        Assert.notNull( id, "id can't be null." );
        TriggerGroup group = mapper.selectByPrimaryKey( id );
        return group;
    }

    @Override
    public List<TriggerGroup> findAll()
    {
        TriggerGroupExample query = new TriggerGroupExample();
        return mapper.selectByExample( query );
    }

    @Override
    public List<TriggerGroupVO> findRegistryGroup()
    {
        List<TriggerGroup> list = findAll();
        List<TriggerGroupVO> vos = Lists.newArrayList();
        if ( CollectionUtils.isNotEmpty( list ) )
        {
            for ( TriggerGroup group : list )
            {
                TriggerGroupVO vo = BeanMapper.map( group, TriggerGroupVO.class );
                List<String> registryList = null;
                if ( group.getAddressType() == 0 )
                {
                    registryList = JobRegistryMonitor.discover( RegistryConfig.RegistType.EXECUTOR.name(),
                            group.getAppName() );
                }
                else
                {
                    if ( StringUtils.isNotBlank( group.getAddressList() ) )
                    {
                        registryList = Arrays.asList( group.getAddressList().split( "," ) );
                    }
                }
                vo.setRegistryList( registryList );
                vos.add( vo );
            }
        }
        return vos;
    }

    @Override
    public boolean save( TriggerGroupReq req )
    {
        Assert.notNull( req, "req can't be null." );
        List<TriggerGroup> groups = findByAppName( req.getAppName() );
        if ( CollectionUtils.isNotEmpty( groups ) )
        {
            return false;
        }
        TriggerGroup o = BeanMapper.map( req, TriggerGroup.class );
        int res = mapper.insert( o );
        if ( res == 1 )
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean update( TriggerGroupReq req )
    {
        Assert.notNull( req, "req can't be null." );
        TriggerGroup old = get( req.getId() );
        if ( old == null )
        {
            return false;
        }
        if (!old.getAppName().equalsIgnoreCase( req.getAppName() )) {
            List<TriggerGroup> groups = findByAppName( req.getAppName() );
            if ( CollectionUtils.isNotEmpty( groups ) )
            {
                return false;
            }
        }
        BeanMapper.copy( req, old );
        int res = mapper.updateByPrimaryKey( old );
        if ( res == 1 )
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean remove( long id )
    {
        TriggerGroup old = get( id );
        if ( old == null )
        {
            return false;
        }
        int res = mapper.deleteByPrimaryKey( id );
        if ( res == 1 )
        {
            return true;
        }
        return false;
    }

    /**
     * 通过appName获取处理器列表
     *
     * @param appName
     * @return
     */
    private List<TriggerGroup> findByAppName( String appName )
    {
        TriggerGroupExample query = new TriggerGroupExample();
        if ( appName != null )
        {
            query.createCriteria().andAppNameEqualTo( appName );
            return mapper.selectByExample( query );
        }
        return null;
    }

}
