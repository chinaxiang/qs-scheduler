/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.enums;

/**
 * 执行失败处理策略
 * 
 * @author yanxiang.huang 2017-07-10 10:11:35
 */
public enum ExecutorFailStrategyEnum
{

    /** 失败告警 */
    FAIL_ALARM( "失败告警" ),

    /** 失败重试 */
    FAIL_RETRY( "失败重试" );

    private final String title;

    private ExecutorFailStrategyEnum( String title )
    {
        this.title = title;
    }

    public static ExecutorFailStrategyEnum match( String name, ExecutorFailStrategyEnum defaultItem )
    {
        if ( name != null )
        {
            for ( ExecutorFailStrategyEnum item : ExecutorFailStrategyEnum.values() )
            {
                if ( item.name().equals( name ) )
                {
                    return item;
                }
            }
        }
        return defaultItem;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

}
