/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.service;

import java.util.List;

import com.quanshi.scheduler.job.biz.model.req.TriggerGroupReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerGroupVO;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;

/**
 * 执行器表处理逻辑
 * 
 * @author yanxiang.huang 2017-07-10 10:23:00
 */
public interface TriggerGroupService
{

    /**
     * 通过ID获取处理器
     *
     * @param id
     * @return
     */
    TriggerGroup get( Long id );

    /**
     * 获取所有触发器组
     *
     */
    List<TriggerGroup> findAll();

    /**
     * 获取所有触发器分组及其可用地址列表
     *
     * @return
     */
    List<TriggerGroupVO> findRegistryGroup();

    /**
     * 新建处理器分组
     *
     * @param req
     * @return
     */
    boolean save( TriggerGroupReq req );

    /**
     * 更新分组
     *
     * @param req
     * @return
     */
    boolean update( TriggerGroupReq req );

    /**
     * 删除分组
     *
     * @param id
     * @return
     */
    boolean remove( long id );

}
