/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.route.strategy;

import java.util.ArrayList;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.ExecutorBiz;
import com.quanshi.scheduler.core.biz.model.TriggerParam;
import com.quanshi.scheduler.core.rpc.netcom.jetty.NetComClientProxy;
import com.quanshi.scheduler.job.biz.route.ExecutorRouter;
import com.quanshi.scheduler.job.dal.entity.TriggerLog;

/**
 * 忙碌转移
 * 
 * @author yanxiang.huang 2017-07-10 13:25:36
 */
public class ExecutorRouteBusyover extends ExecutorRouter
{

    @Override
    public String route( long jobId, ArrayList<String> addressList )
    {
        return addressList.get(0);
    }

    @Override
    public Ret<String> routeRun( TriggerParam triggerParam, ArrayList<String> addressList, TriggerLog jobLog )
    {
        StringBuffer idleBeatResultSB = new StringBuffer();
        for (String address : addressList) {
            // beat
            Ret<String> idleBeatResult = null;
            try {
                ExecutorBiz executorBiz = (ExecutorBiz) new NetComClientProxy(ExecutorBiz.class, address).getObject();
                idleBeatResult = executorBiz.idleBeat(triggerParam.getJobId());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                idleBeatResult = new Ret<String>(Ret.FAIL_CODE, e.getMessage() );
            }
            idleBeatResultSB.append("<br>----------------------<br>")
                    .append("空闲检测：")
                    .append("<br>address：").append(address)
                    .append("<br>code：").append(idleBeatResult.getCode())
                    .append("<br>msg：").append(idleBeatResult.getMsg());

            // beat success
            if (idleBeatResult.getCode() == Ret.SUCCESS_CODE) {
                jobLog.setExecutorAddress(address);

                Ret<String> runResult = runExecutor(triggerParam, address);
                idleBeatResultSB.append("<br>----------------------<br>").append(runResult.getMsg());

                return new Ret<String>(runResult.getCode(), idleBeatResultSB.toString());
            }
        }

        return new Ret<String>(Ret.FAIL_CODE, idleBeatResultSB.toString());
    }

}
