/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.biz.mapper;

import com.quanshi.scheduler.job.biz.model.vo.TriggerInfoVO;
import com.quanshi.scheduler.job.biz.utils.BeanMapper;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;

/**
 * 
 * @author yanxiang.huang 2017-07-13 16:05:18
 */
public class JobInfoMapper
{
    
    private JobInfoMapper() {}
    
    public static TriggerInfoVO do2vo(TriggerInfo o) {
        TriggerInfoVO vo = new TriggerInfoVO();
        BeanMapper.copy( o, vo );
        return vo;
    }

}
