/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.form;

import java.io.Serializable;

/**
 * 
 * @author yanxiang.huang 2017-07-17 10:45:09
 */
public class JobLogPageForm implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = -2602020278439885993L;

    private Long jobGroup;

    private Long jobId;
    
    private String filterTime;

    private int start = 0;

    private int length = 10;

    /**
     * @return the jobGroup
     */
    public Long getJobGroup()
    {
        return jobGroup;
    }

    /**
     * @param jobGroup the jobGroup to set
     */
    public void setJobGroup( Long jobGroup )
    {
        this.jobGroup = jobGroup;
    }

    /**
     * @return the jobId
     */
    public Long getJobId()
    {
        return jobId;
    }

    /**
     * @param jobId the jobId to set
     */
    public void setJobId( Long jobId )
    {
        this.jobId = jobId;
    }

    /**
     * @return the filterTime
     */
    public String getFilterTime()
    {
        return filterTime;
    }

    /**
     * @param filterTime the filterTime to set
     */
    public void setFilterTime( String filterTime )
    {
        this.filterTime = filterTime;
    }

    /**
     * @return the start
     */
    public int getStart()
    {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart( int start )
    {
        this.start = start;
    }

    /**
     * @return the length
     */
    public int getLength()
    {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength( int length )
    {
        this.length = length;
    }

}
