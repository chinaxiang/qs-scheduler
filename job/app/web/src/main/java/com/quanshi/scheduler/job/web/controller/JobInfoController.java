/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.enums.ExecutorBlockStrategyEnum;
import com.quanshi.scheduler.core.enums.GlueTypeEnum;
import com.quanshi.scheduler.job.biz.enums.ExecutorFailStrategyEnum;
import com.quanshi.scheduler.job.biz.enums.ExecutorRouteStrategyEnum;
import com.quanshi.scheduler.job.biz.model.req.TriggerInfoReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerInfoVO;
import com.quanshi.scheduler.job.biz.service.JobService;
import com.quanshi.scheduler.job.biz.service.TriggerGroupService;
import com.quanshi.scheduler.job.biz.service.TriggerInfoService;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;
import com.quanshi.scheduler.job.web.form.JobInfoForm;
import com.quanshi.scheduler.job.web.form.JobInfoPageForm;
import com.quanshi.scheduler.job.web.mapper.JobInfoMapper;

/**
 * 
 * @author yanxiang.huang 2017-07-13 14:51:20
 */
@Controller
@RequestMapping( "/jobinfo" )
public class JobInfoController extends BaseController
{

    @Resource
    private TriggerGroupService triggerGroupService;

    @Resource
    private TriggerInfoService triggerInfoService;

    @Resource
    private JobService jobService;

    @RequestMapping
    public String index( Model model )
    {
        logger.info( "access /jobinfo" );
        // 枚举-字典
        model.addAttribute( "executorRouteStrategyEnum", ExecutorRouteStrategyEnum.values() ); // 路由策略-列表
        model.addAttribute( "glueTypeEnum", GlueTypeEnum.values() ); // Glue类型-字典
        model.addAttribute( "executorBlockStrategyEnum", ExecutorBlockStrategyEnum.values() ); // 阻塞处理策略-字典
        model.addAttribute( "executorFailStrategyEnum", ExecutorFailStrategyEnum.values() ); // 失败处理策略-字典
        // 任务组
        List<TriggerGroup> jobGroupList = triggerGroupService.findAll();
        model.addAttribute( "jobGroupList", jobGroupList );
        return "jobinfo/index";
    }

    @RequestMapping( "/page" )
    @ResponseBody
    public Ret<PageInfo<TriggerInfoVO>> page( JobInfoPageForm form )
    {
        logger.info( "access /jobinfo/page" );
        TriggerInfoReq req = JobInfoMapper.pageForm2Req( form );
        return triggerInfoService.findPage( req );
    }

    @RequestMapping( "/pageList" )
    @ResponseBody
    public Map<String, Object> pageList( JobInfoPageForm form )
    {
        logger.info( "access /jobinfo/pageList" );
        TriggerInfoReq req = JobInfoMapper.pageForm2Req( form );
        return triggerInfoService.findPageList( req );
    }

    /**
     * 新增任务
     *
     * @param form
     * @return
     */
    @RequestMapping( "/add" )
    @ResponseBody
    public Ret<String> add( JobInfoForm form )
    {
        logger.info( "access /jobinfo/add" );
        TriggerInfo jobInfo = JobInfoMapper.form2do( form );
        boolean res = jobService.add( jobInfo );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    /**
     * 修改任务
     *
     * @param form
     * @return
     */
    @RequestMapping( "/reschedule" )
    @ResponseBody
    public Ret<String> reschedule( JobInfoForm form )
    {
        logger.info( "access /jobinfo/reschedule" );
        TriggerInfo jobInfo = JobInfoMapper.form2do( form );
        boolean res = jobService.reschedule( jobInfo );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    /**
     * 删除任务
     *
     * @param id
     * @return
     */
    @RequestMapping( "/remove" )
    @ResponseBody
    public Ret<String> remove( long id )
    {
        logger.info( "access /jobinfo/remove" );
        boolean res = jobService.remove( id );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    /**
     * 暂停任务
     *
     * @param id
     * @return
     */
    @RequestMapping( "/pause" )
    @ResponseBody
    public Ret<String> pause( long id )
    {
        logger.info( "access /jobinfo/pause" );
        boolean res = jobService.pause( id );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    /**
     * 恢复任务
     *
     * @param id
     * @return
     */
    @RequestMapping( "/resume" )
    @ResponseBody
    public Ret<String> resume( long id )
    {
        logger.info( "access /jobinfo/resume" );
        boolean res = jobService.resume( id );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    /**
     * 执行任务
     *
     * @param id
     * @return
     */
    @RequestMapping( "/trigger" )
    @ResponseBody
    public Ret<String> triggerJob( long id )
    {
        logger.info( "access /jobinfo/trigger" );
        boolean res = jobService.triggerJob( id );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }
}
