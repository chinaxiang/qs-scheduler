/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.mapper;

import com.quanshi.scheduler.job.biz.model.req.TriggerInfoReq;
import com.quanshi.scheduler.job.biz.utils.BeanMapper;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;
import com.quanshi.scheduler.job.web.form.JobInfoForm;
import com.quanshi.scheduler.job.web.form.JobInfoPageForm;

/**
 * 
 * @author yanxiang.huang 2017-07-13 16:00:46
 */
public class JobInfoMapper
{
    
    private JobInfoMapper() {}
    
    /**
     * 
     *
     * @param form
     * @return
     */
    public static TriggerInfoReq pageForm2Req(JobInfoPageForm form) {
        if (form == null) {
            return null;
        }
        TriggerInfoReq req = new TriggerInfoReq();
        req.setJobGroup( form.getJobGroup() );
        req.setAuthor( form.getAuthor() );
        req.setExecutorHandler( form.getExecutorHandler() );
        req.setExecutorRouteStrategy( form.getExecutorRouteStrategy() );
        req.setExecutorBlockStrategy( form.getExecutorBlockStrategy() );
        req.setExecutorFailStrategy( form.getExecutorFailStrategy() );
        req.setGlueType( form.getGlueType() );
        req.setPageNo( form.getPageNo() );
        req.setPageSize( form.getPageSize() );
        req.setStart( form.getStart() );
        req.setLength( form.getLength() );
        return req;
    }

    /**
     * 
     *
     * @param form
     * @return
     */
    public static TriggerInfo form2do( JobInfoForm form )
    {
        if (form == null) {
            return null;
        }
        TriggerInfo info = new TriggerInfo();
        BeanMapper.copy( form, info );
        return info;
    }

}
