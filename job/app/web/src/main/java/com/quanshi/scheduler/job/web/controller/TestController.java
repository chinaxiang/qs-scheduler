///**
// * QUANSHI.com Inc.
// * Copyright (c) 2016-2017 All Rights Reserved.
// */
//package com.quanshi.scheduler.job.web.controller;
//
//import javax.annotation.Resource;
//
//import org.springframework.context.annotation.Lazy;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.quanshi.scheduler.job.facade.dto.Secret;
//import com.quanshi.scheduler.job.integration.DubboMonitor;
//
///**
// * 
// * @author yanxiang.huang 2017-07-27 15:01:41
// */
//@RestController
//@RequestMapping( "/test" )
//@Lazy(value = true)
//public class TestController extends BaseController
//{
//
//    @Resource
//    private DubboMonitor monitor;
//
//    @RequestMapping( "/tell" )
//    public Secret tell( String msg )
//    {
//        return monitor.tell( msg );
//    }
//}
