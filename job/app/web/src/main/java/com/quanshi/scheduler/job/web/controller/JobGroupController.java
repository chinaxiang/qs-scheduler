/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.job.biz.model.req.TriggerGroupReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerGroupVO;
import com.quanshi.scheduler.job.biz.service.TriggerGroupService;
import com.quanshi.scheduler.job.biz.service.TriggerInfoService;
import com.quanshi.scheduler.job.biz.utils.BeanMapper;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.web.form.JobGroupForm;

/**
 * 
 * @author yanxiang.huang 2017-07-14 15:14:55
 */
@Controller
@RequestMapping( "/jobgroup" )
public class JobGroupController extends BaseController
{

    @Resource
    private TriggerGroupService triggerGroupService;

    @Resource
    private TriggerInfoService triggerInfoService;

    @RequestMapping
    public String index( Model model )
    {
        logger.info( "access /jobgroup" );
        List<TriggerGroupVO> list = triggerGroupService.findRegistryGroup();
        model.addAttribute( "list", list );
        return "jobgroup/index";
    }

    @RequestMapping( "/save" )
    @ResponseBody
    public Ret<String> save( JobGroupForm form )
    {
        logger.info( "access /jobgroup/save" );
        if ( form == null )
        {
            return new Ret<String>( Ret.FAIL_CODE, "form can't be null." );
        }
        if ( StringUtils.isBlank( form.getAppName() ) )
        {
            return new Ret<String>( Ret.FAIL_CODE, "请输入AppName" );
        }
        if ( form.getAppName().length() > 64 || form.getAppName().length() < 4 )
        {
            return new Ret<String>( Ret.FAIL_CODE, "AppName长度限制为4~64" );
        }
        if ( StringUtils.isBlank( form.getTitle() ) )
        {
            return new Ret<String>( Ret.FAIL_CODE, "请输入名称" );
        }
        if ( form.getAddressType() != 0 )
        {
            if ( StringUtils.isBlank( form.getAddressList() ) )
            {
                return new Ret<String>( Ret.FAIL_CODE, "手动录入注册方式，机器地址不可为空" );
            }
            String[] addresss = form.getAddressList().split( "," );
            for ( String item : addresss )
            {
                if ( StringUtils.isBlank( item ) )
                {
                    return new Ret<String>( Ret.FAIL_CODE, "机器地址非法" );
                }
            }
        }
        TriggerGroupReq req = BeanMapper.map( form, TriggerGroupReq.class );
        boolean res = triggerGroupService.save( req );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    @RequestMapping( "/update" )
    @ResponseBody
    public Ret<String> update( JobGroupForm form )
    {
        logger.info( "access /jobgroup/update" );
        if ( form == null )
        {
            return new Ret<String>( Ret.FAIL_CODE, "form can't be null." );
        }
        if ( form.getId() == null )
        {
            return new Ret<String>( Ret.FAIL_CODE, "id can't be null." );
        }
        if ( StringUtils.isBlank( form.getAppName() ) )
        {
            return new Ret<String>( Ret.FAIL_CODE, "请输入AppName" );
        }
        if ( form.getAppName().length() > 64 || form.getAppName().length() < 4 )
        {
            return new Ret<String>( Ret.FAIL_CODE, "AppName长度限制为4~64" );
        }
        if ( StringUtils.isBlank( form.getTitle() ) )
        {
            return new Ret<String>( Ret.FAIL_CODE, "请输入名称" );
        }
        if ( form.getAddressType() != 0 )
        {
            if ( StringUtils.isBlank( form.getAddressList() ) )
            {
                return new Ret<String>( Ret.FAIL_CODE, "手动录入注册方式，机器地址不可为空" );
            }
            String[] addresss = form.getAddressList().split( "," );
            for ( String item : addresss )
            {
                if ( StringUtils.isBlank( item ) )
                {
                    return new Ret<String>( Ret.FAIL_CODE, "机器地址非法" );
                }
            }
        }
        TriggerGroupReq req = BeanMapper.map( form, TriggerGroupReq.class );
        boolean res = triggerGroupService.update( req );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

    @RequestMapping( "/remove" )
    @ResponseBody
    public Ret<String> remove( long id )
    {
        logger.info( "access /jobgroup/remove" );
        boolean res = triggerInfoService.checkHasUse( id );
        if ( res )
        {
            return new Ret<String>( Ret.FAIL_CODE, "该分组使用中, 不可删除" );
        }

        List<TriggerGroup> list = triggerGroupService.findAll();
        if ( list.size() == 1 )
        {
            return new Ret<String>( Ret.FAIL_CODE, "删除失败, 系统需要至少预留一个默认分组" );
        }
        res = triggerGroupService.remove( id );
        if ( res )
        {
            return Ret.SUCCESS;
        }
        return Ret.FAIL;
    }

}
