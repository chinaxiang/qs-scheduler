/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.model.LogResult;
import com.quanshi.scheduler.job.biz.model.req.TriggerLogReq;
import com.quanshi.scheduler.job.biz.model.vo.TriggerInfoVO;
import com.quanshi.scheduler.job.biz.service.TriggerGroupService;
import com.quanshi.scheduler.job.biz.service.TriggerInfoService;
import com.quanshi.scheduler.job.biz.service.TriggerLogService;
import com.quanshi.scheduler.job.dal.entity.TriggerGroup;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;
import com.quanshi.scheduler.job.dal.entity.TriggerLog;
import com.quanshi.scheduler.job.web.form.JobLogPageForm;
import com.quanshi.scheduler.job.web.mapper.JobLogMapper;

/**
 * 
 * @author yanxiang.huang 2017-07-17 09:22:55
 */
@Controller
@RequestMapping( "/joblog" )
public class JobLogController extends BaseController
{

    @Resource
    private TriggerGroupService triggerGroupService;

    @Resource
    private TriggerInfoService triggerInfoService;

    @Resource
    private TriggerLogService triggerLogService;

    @RequestMapping
    public String index( Model model, Long jobId )
    {
        // 执行器列表
        List<TriggerGroup> groups = triggerGroupService.findAll();
        model.addAttribute( "jobGroupList", groups );
        // 任务
        if ( jobId != null && jobId > 0 )
        {
            TriggerInfo jobInfo = triggerInfoService.get( jobId );
            model.addAttribute( "jobInfo", jobInfo );
        }
        return "joblog/index";
    }

    @RequestMapping( "/getJobsByGroup" )
    @ResponseBody
    public Ret<?> getJobByGroup( Long jobGroup )
    {
        if ( jobGroup == null || jobGroup < 0 )
        {
            return Ret.FAIL;
        }
        List<TriggerInfoVO> list = Lists.newArrayList();
        if (jobGroup > 0) {
            List<TriggerInfoVO> datas = triggerInfoService.getByGroup( jobGroup );
            if ( CollectionUtils.isNotEmpty( datas ) )
            {
                list = datas;
            }
        }
        return new Ret<List<TriggerInfoVO>>( list );
    }

    @RequestMapping( "/pageList" )
    @ResponseBody
    public Map<String, Object> pageList( JobLogPageForm form )
    {
        TriggerLogReq req = JobLogMapper.pageForm2Req( form );
        return triggerLogService.findDataPage( req );
    }

    @RequestMapping( "/logDetailPage" )
    public String logDetailPage( Long id, Model model )
    {
        if ( id == null || id < 1 )
        {
            throw new RuntimeException( "error log id." );
        }
        TriggerLog log = triggerLogService.get( id );
        model.addAttribute( "triggerCode", log.getTriggerCode() );
        model.addAttribute( "handleCode", log.getHandleCode() );
        model.addAttribute( "executorAddress", log.getExecutorAddress() );
        model.addAttribute( "triggerTime", log.getTriggerTime().getTime() );
        model.addAttribute( "logId", log.getId() );
        return "joblog/detail";
    }

    @RequestMapping( "/logDetailCat" )
    @ResponseBody
    public Ret<LogResult> logDetailCat( String executorAddress, long triggerTime, long logId, int fromLineNum )
    {
        return triggerLogService.catLog( executorAddress, triggerTime, logId, fromLineNum );
    }

    @RequestMapping( "/logKill" )
    @ResponseBody
    public Ret<?> killJob( Long id )
    {
        if ( id == null || id < 1 )
        {
            return new Ret<String>( Ret.FAIL_CODE, "error log id." );
        }
        TriggerLog log = triggerLogService.get( id );
        TriggerInfo jobInfo = triggerInfoService.get( log.getJobId() );
        if ( jobInfo == null )
        {
            return new Ret<>( Ret.FAIL_CODE, "param invalid." );
        }
        if ( log.getTriggerCode() != Ret.SUCCESS_CODE )
        {
            return new Ret<>( Ret.FAIL_CODE, "trigger fail, don't need kill." );
        }
        return triggerLogService.killLog( log );
    }

    @RequestMapping( "/clearLog" )
    @ResponseBody
    public Ret<?> clearLog( Long jobGroup, Long jobId, int type )
    {
        Date clearBeforeTime = null;
        int clearBeforeNum = 0;
        if ( type == 1 )
        {
            clearBeforeTime = DateUtils.addMonths( new Date(), -1 ); // 清理一个月之前日志数据
        }
        else if ( type == 2 )
        {
            clearBeforeTime = DateUtils.addMonths( new Date(), -3 ); // 清理三个月之前日志数据
        }
        else if ( type == 3 )
        {
            clearBeforeTime = DateUtils.addMonths( new Date(), -6 ); // 清理六个月之前日志数据
        }
        else if ( type == 4 )
        {
            clearBeforeTime = DateUtils.addYears( new Date(), -1 ); // 清理一年之前日志数据
        }
        else if ( type == 5 )
        {
            clearBeforeNum = 1000; // 清理一千条以前日志数据
        }
        else if ( type == 6 )
        {
            clearBeforeNum = 10000; // 清理一万条以前日志数据
        }
        else if ( type == 7 )
        {
            clearBeforeNum = 30000; // 清理三万条以前日志数据
        }
        else if ( type == 8 )
        {
            clearBeforeNum = 100000; // 清理十万条以前日志数据
        }
        else if ( type == 9 )
        {
            clearBeforeNum = 0; // 清理所用日志数据
        }
        else
        {
            return new Ret<>( Ret.FAIL_CODE, "clear log type invalid." );
        }

        triggerLogService.clearLog( jobGroup, jobId, clearBeforeTime, clearBeforeNum );
        return Ret.SUCCESS;
    }

}
