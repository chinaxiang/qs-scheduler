/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * 
 * @author yanxiang.huang 2017-07-11 13:31:43
 */
public abstract class BaseController implements InitializingBean
{

    protected Logger logger = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void afterPropertiesSet() throws Exception
    {
    }

}
