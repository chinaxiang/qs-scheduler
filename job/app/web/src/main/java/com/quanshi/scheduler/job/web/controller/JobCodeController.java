/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.enums.GlueTypeEnum;
import com.quanshi.scheduler.job.biz.service.TriggerInfoService;
import com.quanshi.scheduler.job.biz.service.TriggerLogglueService;
import com.quanshi.scheduler.job.dal.entity.TriggerInfo;
import com.quanshi.scheduler.job.dal.entity.TriggerLogglue;

/**
 * 
 * @author yanxiang.huang 2017-07-14 14:01:40
 */
@Controller
@RequestMapping( "/jobcode" )
public class JobCodeController extends BaseController
{

    @Resource
    private TriggerInfoService triggerInfoService;

    @Resource
    private TriggerLogglueService triggerLogglueService;

    @RequestMapping
    public String index( Model model, long jobId )
    {
        logger.info( "access /jobcode jobId:{}.", jobId );
        TriggerInfo jobInfo = triggerInfoService.get( jobId );
        if ( jobInfo == null )
        {
            throw new RuntimeException( "job info can't be found." );
        }
        if ( GlueTypeEnum.BEAN == GlueTypeEnum.match( jobInfo.getGlueType() ) )
        {
            throw new RuntimeException( "job glue type is a bean type." );
        }
        List<TriggerLogglue> logs = triggerLogglueService.findByJobId( jobId );
        model.addAttribute( "glueTypeEnum", GlueTypeEnum.values() );
        model.addAttribute( "jobInfo", jobInfo );
        model.addAttribute( "jobLogGlues", logs );
        return "jobcode/index";
    }

    @RequestMapping( "/save" )
    @ResponseBody
    public Ret<String> save( Model model, long jobId, String glueSource, String glueRemark )
    {
        logger.info( "access /jobcode/save jobId:{}, glueRemark:{}.", jobId, glueRemark );
        if ( glueRemark == null )
        {
            return new Ret<String>( Ret.FAIL_CODE, "please input remark." );
        }
        if ( glueRemark.length() < 4 || glueRemark.length() > 100 )
        {
            return new Ret<String>( Ret.FAIL_CODE, "remark length between [4-100]." );
        }
        TriggerInfo jobInfo = triggerInfoService.get( jobId );
        if ( jobInfo == null )
        {
            return new Ret<String>( Ret.FAIL_CODE, "job info can't be found." );
        }
        jobInfo.setGlueRemark( glueRemark );
        jobInfo.setGlueSource( glueSource );
        boolean res = triggerInfoService.updateGlue( jobInfo );
        if ( res )
        {
            triggerLogglueService.saveLog( jobId, jobInfo.getGlueType(), glueRemark, glueSource );
            return Ret.SUCCESS;
        }
        return new Ret<String>( Ret.FAIL_CODE, "save glue source fail." );
    }

}
