/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.quanshi.scheduler.job.biz.service.JobService;

/**
 * 
 * @author yanxiang.huang 2017-07-12 19:51:03
 */
@Controller
public class IndexController extends BaseController
{

    @Resource
    private JobService jobService;

    @RequestMapping( "/" )
    public String index( Model model )
    {
        logger.info( "access /" );
        Map<String, Object> dashboardMap = jobService.dashboard();
        model.addAllAttributes( dashboardMap );
        return "index";
    }

    @RequestMapping( "/help" )
    public String help()
    {
        logger.info( "access /help" );
        return "help";
    }
}
