/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.util.ServletContextPropertyUtils;
import org.springframework.web.util.WebUtils;

/**
 * 
 * @author yanxiang.huang 2017-07-11 14:17:13
 */
public class ResourceFileListener implements ServletContextListener
{
    
    private Logger logger = LoggerFactory.getLogger( ResourceFileListener.class );
    
    public static final String RS_CONF_LOCATION         = "rsConfLocation";

    public static final String DEFAULT_RS_CONF_LOCATION = "classpath:application.properties";

    @Override
    public void contextInitialized( ServletContextEvent sce )
    {
        logger.info( "servlet context initialized." );
        ServletContext context = sce.getServletContext();
        String location = context.getInitParameter(RS_CONF_LOCATION);
        if (StringUtils.isBlank(location)) {
            location = DEFAULT_RS_CONF_LOCATION;
        }
        try {
            String[] locs = StringUtils.split( location );
            for ( String s : locs )
            {
                location = ServletContextPropertyUtils.resolvePlaceholders(s, context);
                if (!ResourceUtils.isUrl(location)) {
                    location = WebUtils.getRealPath(context, location);
                    
                    String osName = System.getProperty("os.name");
                    if (osName.toLowerCase().indexOf("windows") >= 0) {
                        location = "/" + location;
                    }
                }
                File file = ResourceUtils.getFile( location );
                FileInputStream fis = new FileInputStream( file );
                Properties pro = new Properties();  
                pro.load(fis);
                
                Set<Entry<Object, Object>> set = pro.entrySet();
                for ( Entry<Object, Object> entry : set )
                {
                    String key = ( String ) entry.getKey();
                    String value = ( String ) entry.getValue();
                    logger.info( "{}={}.", key, value );
                    context.setAttribute( key, value );
                }
                fis.close();
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Invalid 'rsConfLocation' parameter: " + ex.getMessage());
        }
    }

    @Override
    public void contextDestroyed( ServletContextEvent sce )
    {
        // no-op
        logger.info( "context destroyed." );
    }

}
