/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.form;

import java.io.Serializable;

/**
 * 
 * @author yanxiang.huang 2017-07-14 08:54:26
 */
public class JobInfoForm implements Serializable
{

    /** serialVersionUID */
    private static final long serialVersionUID = 3709942116139451046L;

    private Long id;

    private Long jobGroup;

    private String jobCron;

    private String jobDesc;

    private String author;

    private String alarmEmail;

    private String executorRouteStrategy;

    private String executorHandler;

    private String executorParam;

    private String executorBlockStrategy;

    private String executorFailStrategy;

    private String glueType;

    private String glueRemark;

    private String childJobkey;

    private String glueSource;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    /**
     * @return the jobGroup
     */
    public Long getJobGroup()
    {
        return jobGroup;
    }

    /**
     * @param jobGroup the jobGroup to set
     */
    public void setJobGroup( Long jobGroup )
    {
        this.jobGroup = jobGroup;
    }

    /**
     * @return the jobCron
     */
    public String getJobCron()
    {
        return jobCron;
    }

    /**
     * @param jobCron the jobCron to set
     */
    public void setJobCron( String jobCron )
    {
        this.jobCron = jobCron;
    }

    /**
     * @return the jobDesc
     */
    public String getJobDesc()
    {
        return jobDesc;
    }

    /**
     * @param jobDesc the jobDesc to set
     */
    public void setJobDesc( String jobDesc )
    {
        this.jobDesc = jobDesc;
    }

    /**
     * @return the author
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor( String author )
    {
        this.author = author;
    }

    /**
     * @return the alarmEmail
     */
    public String getAlarmEmail()
    {
        return alarmEmail;
    }

    /**
     * @param alarmEmail the alarmEmail to set
     */
    public void setAlarmEmail( String alarmEmail )
    {
        this.alarmEmail = alarmEmail;
    }

    /**
     * @return the executorRouteStrategy
     */
    public String getExecutorRouteStrategy()
    {
        return executorRouteStrategy;
    }

    /**
     * @param executorRouteStrategy the executorRouteStrategy to set
     */
    public void setExecutorRouteStrategy( String executorRouteStrategy )
    {
        this.executorRouteStrategy = executorRouteStrategy;
    }

    /**
     * @return the executorHandler
     */
    public String getExecutorHandler()
    {
        return executorHandler;
    }

    /**
     * @param executorHandler the executorHandler to set
     */
    public void setExecutorHandler( String executorHandler )
    {
        this.executorHandler = executorHandler;
    }

    /**
     * @return the executorParam
     */
    public String getExecutorParam()
    {
        return executorParam;
    }

    /**
     * @param executorParam the executorParam to set
     */
    public void setExecutorParam( String executorParam )
    {
        this.executorParam = executorParam;
    }

    /**
     * @return the executorBlockStrategy
     */
    public String getExecutorBlockStrategy()
    {
        return executorBlockStrategy;
    }

    /**
     * @param executorBlockStrategy the executorBlockStrategy to set
     */
    public void setExecutorBlockStrategy( String executorBlockStrategy )
    {
        this.executorBlockStrategy = executorBlockStrategy;
    }

    /**
     * @return the executorFailStrategy
     */
    public String getExecutorFailStrategy()
    {
        return executorFailStrategy;
    }

    /**
     * @param executorFailStrategy the executorFailStrategy to set
     */
    public void setExecutorFailStrategy( String executorFailStrategy )
    {
        this.executorFailStrategy = executorFailStrategy;
    }

    /**
     * @return the glueType
     */
    public String getGlueType()
    {
        return glueType;
    }

    /**
     * @param glueType the glueType to set
     */
    public void setGlueType( String glueType )
    {
        this.glueType = glueType;
    }

    /**
     * @return the glueRemark
     */
    public String getGlueRemark()
    {
        return glueRemark;
    }

    /**
     * @param glueRemark the glueRemark to set
     */
    public void setGlueRemark( String glueRemark )
    {
        this.glueRemark = glueRemark;
    }

    /**
     * @return the childJobkey
     */
    public String getChildJobkey()
    {
        return childJobkey;
    }

    /**
     * @param childJobkey the childJobkey to set
     */
    public void setChildJobkey( String childJobkey )
    {
        this.childJobkey = childJobkey;
    }

    /**
     * @return the glueSource
     */
    public String getGlueSource()
    {
        return glueSource;
    }

    /**
     * @param glueSource the glueSource to set
     */
    public void setGlueSource( String glueSource )
    {
        this.glueSource = glueSource;
    }

}
