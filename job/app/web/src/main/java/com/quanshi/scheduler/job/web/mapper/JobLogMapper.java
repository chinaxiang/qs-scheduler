/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.job.web.mapper;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.quanshi.scheduler.core.utils.DateHelper;
import com.quanshi.scheduler.job.biz.model.req.TriggerLogReq;
import com.quanshi.scheduler.job.web.form.JobLogPageForm;

/**
 * 
 * @author yanxiang.huang 2017-07-17 11:06:43
 */
public class JobLogMapper
{
    
    public static TriggerLogReq pageForm2Req(JobLogPageForm form) {
        if (form == null) {
            return null;
        }
        TriggerLogReq req = new TriggerLogReq();
        req.setJobGroup( form.getJobGroup() );
        req.setJobId( form.getJobId() );
        req.setStart( form.getStart() );
        req.setLength( form.getLength() );
        if (StringUtils.isNotBlank( form.getFilterTime() )) {
            fillDateRange(req, form.getFilterTime());
        }
        return req;
    }

    /**
     * 填充日期范围
     *
     * @param req
     * @param filterTime
     */
    private static void fillDateRange( TriggerLogReq req, String filterTime )
    {
        Date dateStart = null;
        Date dateEnd = null;
        if (StringUtils.isNotBlank(filterTime)) {
            String[] temp = filterTime.split(" - ");
            if (temp.length == 2) {
                dateStart = DateHelper.parse(temp[0]);
                dateEnd = DateHelper.parse(temp[1]);
            }
        }
        req.setDateStart( dateStart );
        req.setDateEnd( dateEnd );
    }

}
