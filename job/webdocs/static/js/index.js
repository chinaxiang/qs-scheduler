$(function () {
	
	var triggingCount = $('#jobLogTriggingCount').text();
	var triggerSucCount = $('#jobLogTriggerSucCount').text();
	var triggerFailCount = $('#jobLogTriggerFailCount').text();
	var runningCount = $('#jobLogRunningCount').text();
	var handleSucCount = $('#jobLogHandleSucCount').text();
	var handleFailCount = $('#jobLogHandleFailCount').text();
	
	/**
     * 饼图1
     */
    function pieChart1Init(data) {
        var option = {
            title : {
                text: '整体状况图',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['正在调度','调度失败','正在运行','运行成功','运行失败']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {
                            value:data.triggingCount,
                            name:'正在调度'
                        },
                        {
                            value:data.triggerFailCount,
                            name:'调度失败'
                        },
						{
                            value:data.runningCount,
                            name:'正在运行'
                        },
						{
                            value:data.handleSucCount,
                            name:'运行成功'
                        },
						{
                            value:data.handleFailCount,
                            name:'运行失败'
                        }
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ],
            color:['#39cccc', '#dd4b39', '#99CCFF', '#00A65A', '#660000' ]
        };
        var pieChart1 = echarts.init(document.getElementById('pieChart1'));
        pieChart1.setOption(option);
    }
	
	var data1 = {
		'triggingCount': triggingCount,
		'triggerFailCount': triggerFailCount,
		'runningCount': runningCount,
		'handleSucCount': handleSucCount,
		'handleFailCount': handleFailCount
	};
	pieChart1Init(data1)
    
    /**
     * 饼图2
     */
    function pieChart2Init(data) {
        var option = {
            title : {
                text: '调度状况图',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['正在调度','调度成功','调度失败']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {
                            value:data.triggingCount,
                            name:'正在调度'
                        },
                        {
                            value:data.triggerSucCount,
                            name:'调度成功'
                        },
						{
                            value:data.triggerFailCount,
                            name:'调度失败'
                        }
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ],
            color:['#39cccc', '#00A65A', '#dd4b39']
        };
        var pieChart2 = echarts.init(document.getElementById('pieChart2'));
        pieChart2.setOption(option);
    }
	
	var data2 = {
		'triggingCount': triggingCount,
		'triggerSucCount': triggerSucCount,
		'triggerFailCount': triggerFailCount
	};
	pieChart2Init(data2)
	
	/**
     * 饼图3
     */
    function pieChart3Init(data) {
        var option = {
            title : {
                text: '运行状况图',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['正在运行','运行成功','运行失败']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {
                            value:data.runningCount,
                            name:'正在运行'
                        },
                        {
                            value:data.handleSucCount,
                            name:'运行成功'
                        },
						{
                            value:data.handleFailCount,
                            name:'运行失败'
                        }
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ],
            color:['#39cccc', '#00A65A', '#dd4b39']
        };
        var pieChart3 = echarts.init(document.getElementById('pieChart3'));
        pieChart3.setOption(option);
    }
	
	var data3 = {
		'runningCount': runningCount,
		'handleSucCount': handleSucCount,
		'handleFailCount': handleFailCount
	};
	pieChart3Init(data3)
});
