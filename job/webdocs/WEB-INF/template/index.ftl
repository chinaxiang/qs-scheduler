<!DOCTYPE html>
<html>
<head>
  	<title>全时 - 云调度</title>
  	<#import "/common/common.macro.ftl" as netCommon>
	<@netCommon.commonStyle />
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<!-- header -->
	<@netCommon.commonHeader />
	<!-- left -->
	<@netCommon.commonLeft "index" />
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>云调度<small>控制台</small></h1>
		</section>

		<!-- Main content -->
		<section class="content">

            <!-- 任务信息 -->
            <div class="row">

                <#-- 任务信息 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">任务数量</span>
                            <span class="info-box-number">${jobInfoCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">系统中配置的任务数量</span>
                        </div>
                    </div>
                </div>

                <#-- 调度信息 -->
                <div class="col-md-4 col-sm-6 col-xs-12" >
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">调度次数</span>
                            <span class="info-box-number" id="jobLogCount">${jobLogCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%" ></div>
                            </div>
                            <span class="progress-description">
                                调度中心触发的调度次数
                                <#-- <#if jobLogCount gt 0>
                                    调度成功率：${(jobLogHandleSucCount*100/jobLogCount)?string("0.00")}<small>%</small>
                                </#if> -->
                            </span>
                        </div>
                    </div>
                </div>

                <#-- 执行器 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-purple">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">执行器数量</span>
                            <span class="info-box-number">${executorCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">执行器机器数量(包括手工配置的)</span>
                        </div>
                    </div>
                </div>
				
				<#-- 正在调度 -->
                <div class="col-md-4 col-sm-6 col-xs-12" >
                    <div class="info-box bg-teal">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">正在调度</span>
                            <span class="info-box-number" id="jobLogTriggingCount">${jobLogTriggingCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%" ></div>
                            </div>
                            <span class="progress-description">
                                正在调度的数量
                            </span>
                        </div>
                    </div>
                </div>
				
				<#-- 调度成功 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">调度成功</span>
                            <span class="info-box-number" id="jobLogTriggerSucCount">${jobLogTriggerSucCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">调度成功的数量</span>
                        </div>
                    </div>
                </div>

                <#-- 调度失败 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">调度失败</span>
                            <span class="info-box-number" id="jobLogTriggerFailCount">${jobLogTriggerFailCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">调度失败的数量</span>
                        </div>
                    </div>
                </div>
				
				<#-- 正在运行 -->
                <div class="col-md-4 col-sm-6 col-xs-12" >
                    <div class="info-box bg-teal">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">正在运行</span>
                            <span class="info-box-number" id="jobLogRunningCount">${jobLogRunningCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%" ></div>
                            </div>
                            <span class="progress-description">
                                正在运行的数量
                            </span>
                        </div>
                    </div>
                </div>
				
				<#-- 运行成功 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">运行成功</span>
                            <span class="info-box-number" id="jobLogHandleSucCount">${jobLogHandleSucCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">运行成功的数量</span>
                        </div>
                    </div>
                </div>

                <#-- 运行失败 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-flag-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">运行失败</span>
                            <span class="info-box-number" id="jobLogHandleFailCount">${jobLogHandleFailCount}</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">运行失败的数量</span>
                        </div>
                    </div>
                </div>

            </div>
			
			<div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div id="pieChart1" style="height: 350px;"></div>
                </div>
				<div class="col-md-4 col-sm-6 col-xs-12">
                    <div id="pieChart2" style="height: 350px;"></div>
                </div>
				<div class="col-md-4 col-sm-6 col-xs-12">
                    <div id="pieChart3" style="height: 350px;"></div>
                </div>
			</div>


		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	
	<!-- footer -->
	<@netCommon.commonFooter />
</div>
<@netCommon.commonScript />
<#--<script src="${request.contextPath}/static/adminlte/plugins/daterangepicker/moment.min.js"></script>
<script src="${request.contextPath}/static/adminlte/plugins/daterangepicker/daterangepicker.js"></script>-->
<script src="${request.contextPath}/static/plugins/echarts/echarts.common.min.js"></script>
<script src="${request.contextPath}/static/js/index.js"></script>

</body>
</html>