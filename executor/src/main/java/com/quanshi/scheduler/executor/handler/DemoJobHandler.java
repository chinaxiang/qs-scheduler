/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.executor.handler;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.handler.IJobHandler;
import com.quanshi.scheduler.core.handler.annotation.JobHandler;
import com.quanshi.scheduler.core.log.IJobLogger;

/**
 * demo job
 * 
 * @author yanxiang.huang 2017-07-12 13:52:11
 */
@JobHandler(value = "demoJobHandler")
@Service
public class DemoJobHandler implements IJobHandler
{

    @Override
    public Ret<String> execute( String... params ) throws Exception
    {
        IJobLogger.log("job run, Hello World.");

        for (int i = 0; i < 5; i++) {
            IJobLogger.log("beat at:" + i);
            TimeUnit.SECONDS.sleep(1);
        }
        return Ret.SUCCESS;
    }

}
