/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.executor;

import java.io.IOException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 
 * @author yanxiang.huang 2017-07-12 13:21:36
 */
public class Application
{

    public static void main( String[] args ) throws IOException
    {
        String basePackage = "com.quanshi.scheduler.executor";
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext( basePackage );
        context.start();
        try {
            while (true) {
                try {
                    Thread.sleep(30 * 1000);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    context.close();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (context != null && context.isRunning()) {
                context.close();
            }
        }
    }
}
