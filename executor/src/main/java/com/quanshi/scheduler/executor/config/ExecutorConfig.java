/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.executor.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

/**
 * 
 * @author yanxiang.huang 2017-07-12 13:17:34
 */
@Configuration
@ImportResource("classpath:job-beans.xml")
public class ExecutorConfig implements InitializingBean
{
    
    private Logger logger = LoggerFactory.getLogger( ExecutorConfig.class );
    
    @Autowired
    private Environment env;

    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull( env, "env can't be null." );
        logger.info( ">>>>>>>load properties........................" );
        logger.info( "job.admin.addresses:{}.", env.getProperty( "job.admin.addresses" ) );
        logger.info( "job.executor.appname:{}.", env.getProperty( "job.executor.appname" ) );
        logger.info( "job.executor.ip:{}.", env.getProperty( "job.executor.ip" ) );
        logger.info( "job.executor.port:{}.", env.getProperty( "job.executor.port" ) );
        logger.info( "job.executor.logpath:{}.", env.getProperty( "job.executor.logpath" ) );
        logger.info( "<<<<<<<load properties........................" );
    }

}
