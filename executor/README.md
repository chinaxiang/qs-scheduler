# readme

需要将应用打包为可执行jar.

jar程序需要将依赖的jar包放置到lib中。

```
mvn dependency:copy-dependencies -DoutputDirectory=target/lib
mvn clean install -DskipTests
```

Dockerfile

```
FROM tomcat-plus
MAINTAINER huangyanxiang
COPY target/lib ./lib/
COPY target/scheduler-executor.jar ./
CMD java -jar scheduler-executor.jar
EXPOSE 9900
```
