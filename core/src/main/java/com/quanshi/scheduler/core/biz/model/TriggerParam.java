/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.biz.model;

import java.io.Serializable;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午10:27:42
 */
public class TriggerParam implements Serializable {

    /**  */
    private static final long serialVersionUID = -7872216130719362671L;
    private long               jobId;

    private String            executorHandler;
    private String            executorParams;
    private String            executorBlockStrategy;

    private String            glueType;
    private String            glueSource;
    private long              glueUpdatetime;

    private long               logId;
    private long              logDateTim;

    /**
     * Getter method for property <tt>jobId</tt>.
     * 
     * @return property value of jobId
     */
    public long getJobId() {
        return jobId;
    }

    /**
     * Setter method for property <tt>jobId</tt>.
     * 
     * @param jobId value to be assigned to property jobId
     */
    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    /**
     * Getter method for property <tt>executorHandler</tt>.
     * 
     * @return property value of executorHandler
     */
    public String getExecutorHandler() {
        return executorHandler;
    }

    /**
     * Setter method for property <tt>executorHandler</tt>.
     * 
     * @param executorHandler value to be assigned to property executorHandler
     */
    public void setExecutorHandler(String executorHandler) {
        this.executorHandler = executorHandler;
    }

    /**
     * Getter method for property <tt>executorParams</tt>.
     * 
     * @return property value of executorParams
     */
    public String getExecutorParams() {
        return executorParams;
    }

    /**
     * Setter method for property <tt>executorParams</tt>.
     * 
     * @param executorParams value to be assigned to property executorParams
     */
    public void setExecutorParams(String executorParams) {
        this.executorParams = executorParams;
    }

    /**
     * Getter method for property <tt>executorBlockStrategy</tt>.
     * 
     * @return property value of executorBlockStrategy
     */
    public String getExecutorBlockStrategy() {
        return executorBlockStrategy;
    }

    /**
     * Setter method for property <tt>executorBlockStrategy</tt>.
     * 
     * @param executorBlockStrategy value to be assigned to property executorBlockStrategy
     */
    public void setExecutorBlockStrategy(String executorBlockStrategy) {
        this.executorBlockStrategy = executorBlockStrategy;
    }

    /**
     * Getter method for property <tt>glueType</tt>.
     * 
     * @return property value of glueType
     */
    public String getGlueType() {
        return glueType;
    }

    /**
     * Setter method for property <tt>glueType</tt>.
     * 
     * @param glueType value to be assigned to property glueType
     */
    public void setGlueType(String glueType) {
        this.glueType = glueType;
    }

    /**
     * Getter method for property <tt>glueSource</tt>.
     * 
     * @return property value of glueSource
     */
    public String getGlueSource() {
        return glueSource;
    }

    /**
     * Setter method for property <tt>glueSource</tt>.
     * 
     * @param glueSource value to be assigned to property glueSource
     */
    public void setGlueSource(String glueSource) {
        this.glueSource = glueSource;
    }

    /**
     * Getter method for property <tt>glueUpdatetime</tt>.
     * 
     * @return property value of glueUpdatetime
     */
    public long getGlueUpdatetime() {
        return glueUpdatetime;
    }

    /**
     * Setter method for property <tt>glueUpdatetime</tt>.
     * 
     * @param glueUpdatetime value to be assigned to property glueUpdatetime
     */
    public void setGlueUpdatetime(long glueUpdatetime) {
        this.glueUpdatetime = glueUpdatetime;
    }

    /**
     * Getter method for property <tt>logId</tt>.
     * 
     * @return property value of logId
     */
    public long getLogId() {
        return logId;
    }

    /**
     * Setter method for property <tt>logId</tt>.
     * 
     * @param logId value to be assigned to property logId
     */
    public void setLogId(long logId) {
        this.logId = logId;
    }

    /**
     * Getter method for property <tt>logDateTim</tt>.
     * 
     * @return property value of logDateTim
     */
    public long getLogDateTim() {
        return logDateTim;
    }

    /**
     * Setter method for property <tt>logDateTim</tt>.
     * 
     * @param logDateTim value to be assigned to property logDateTim
     */
    public void setLogDateTim(long logDateTim) {
        this.logDateTim = logDateTim;
    }

    @Override
    public String toString() {
        return "TriggerParam{" + "jobId=" + jobId + 
                ", executorHandler='" + executorHandler + '\'' + 
                ", executorParams='" + executorParams + '\'' + 
                ", executorBlockStrategy='" + executorBlockStrategy + '\'' + 
                ", glueType='" + glueType + '\'' + 
                ", glueSource='" + glueSource + '\'' + 
                ", glueUpdatetime=" + glueUpdatetime + 
                ", logId=" + logId + 
                ", logDateTim=" + logDateTim + '}';
    }

}
