/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.biz.model;

import java.io.Serializable;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午10:25:30
 */
public class LogResult implements Serializable {

    /**  */
    private static final long serialVersionUID = 5496093971127019527L;

    public LogResult(int fromLineNum, int toLineNum, String logContent, boolean isEnd) {
        this.fromLineNum = fromLineNum;
        this.toLineNum = toLineNum;
        this.logContent = logContent;
        this.isEnd = isEnd;
    }

    private int     fromLineNum;
    private int     toLineNum;
    private String  logContent;
    private boolean isEnd;

    /**
     * Getter method for property <tt>fromLineNum</tt>.
     * 
     * @return property value of fromLineNum
     */
    public int getFromLineNum() {
        return fromLineNum;
    }

    /**
     * Setter method for property <tt>fromLineNum</tt>.
     * 
     * @param fromLineNum value to be assigned to property fromLineNum
     */
    public void setFromLineNum(int fromLineNum) {
        this.fromLineNum = fromLineNum;
    }

    /**
     * Getter method for property <tt>toLineNum</tt>.
     * 
     * @return property value of toLineNum
     */
    public int getToLineNum() {
        return toLineNum;
    }

    /**
     * Setter method for property <tt>toLineNum</tt>.
     * 
     * @param toLineNum value to be assigned to property toLineNum
     */
    public void setToLineNum(int toLineNum) {
        this.toLineNum = toLineNum;
    }

    /**
     * Getter method for property <tt>logContent</tt>.
     * 
     * @return property value of logContent
     */
    public String getLogContent() {
        return logContent;
    }

    /**
     * Setter method for property <tt>logContent</tt>.
     * 
     * @param logContent value to be assigned to property logContent
     */
    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    /**
     * Getter method for property <tt>isEnd</tt>.
     * 
     * @return property value of isEnd
     */
    public boolean isEnd() {
        return isEnd;
    }

    /**
     * Setter method for property <tt>isEnd</tt>.
     * 
     * @param isEnd value to be assigned to property isEnd
     */
    public void setEnd(boolean isEnd) {
        this.isEnd = isEnd;
    }

}
