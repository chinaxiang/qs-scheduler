/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.enums;

/**
 * 调度器执行类型
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午8:17:30
 */
public enum GlueTypeEnum {

                          /** bean */
                          BEAN("Bean模式"),
                          /** groovy */
                          GLUE_GROOVY("Glue模式(Java)"),
                          /** shell */
                          GLUE_SHELL("Glue模式(Shell)"),
                          /** python */
                          GLUE_PYTHON("Glue模式(Python)");

    /**  */
    private String desc;

    /**
     * @param desc
     */
    private GlueTypeEnum(String desc) {
        this.desc = desc;
    }

    public static GlueTypeEnum match(String name) {
        for (GlueTypeEnum item : GlueTypeEnum.values()) {
            if (item.name().equals(name)) {
                return item;
            }
        }
        return null;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     * 
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Setter method for property <tt>desc</tt>.
     * 
     * @param desc value to be assigned to property desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

}
