/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.model.HandleCallbackParam;
import com.quanshi.scheduler.core.utils.AdminApiUtils;

/**
 * 回调线程处理类
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午11:10:43
 */
public class TriggerCallbackThread {

    private static Logger                logger   = LoggerFactory.getLogger(TriggerCallbackThread.class);

    private static TriggerCallbackThread instance = new TriggerCallbackThread();

    public static TriggerCallbackThread getInstance() {
        return instance;
    }

    private LinkedBlockingQueue<HandleCallbackParam> callBackQueue = new LinkedBlockingQueue<HandleCallbackParam>();

    private Thread                                   triggerCallbackThread;
    private boolean                                  toStop        = false;

    public void start() {
        triggerCallbackThread = new Thread(new Runnable() {

            @Override
            public void run() {
                while (!toStop) {
                    try {
                        HandleCallbackParam callback = getInstance().callBackQueue.take();
                        if (callback != null) {

                            // callback list
                            List<HandleCallbackParam> callbackParamList = new ArrayList<HandleCallbackParam>();
                            @SuppressWarnings("unused")
                            int drainToNum = getInstance().callBackQueue.drainTo(callbackParamList);
                            callbackParamList.add(callback);

                            // callback, will retry if error
                            try {
                                Ret<String> callbackResult = AdminApiUtils.callApiFailover(AdminApiUtils.CALLBACK, callbackParamList);
                                logger.debug("job callback, callbackParamList:{}, callbackResult:{}", callbackParamList, callbackResult);
                            } catch (Exception e) {
                                logger.error("job TriggerCallbackThread Exception:", e);
                                //getInstance().callBackQueue.addAll(callbackParamList);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("", e);
                    }
                }
            }
        });
        triggerCallbackThread.setDaemon(true);
        triggerCallbackThread.start();
    }

    public void toStop() {
        toStop = true;
    }

    public static void pushCallBack(HandleCallbackParam callback) {
        getInstance().callBackQueue.add(callback);
        logger.debug("job, push callback request, logId:{}", callback.getLogId());
    }
}
