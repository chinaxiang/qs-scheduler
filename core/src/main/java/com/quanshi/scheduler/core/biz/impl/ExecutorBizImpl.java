/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.biz.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.ExecutorBiz;
import com.quanshi.scheduler.core.biz.model.LogResult;
import com.quanshi.scheduler.core.biz.model.TriggerParam;
import com.quanshi.scheduler.core.enums.ExecutorBlockStrategyEnum;
import com.quanshi.scheduler.core.enums.GlueTypeEnum;
import com.quanshi.scheduler.core.executor.IJobExecutor;
import com.quanshi.scheduler.core.handler.GlueFactory;
import com.quanshi.scheduler.core.handler.IJobHandler;
import com.quanshi.scheduler.core.handler.impl.GlueJobHandler;
import com.quanshi.scheduler.core.handler.impl.ScriptJobHandler;
import com.quanshi.scheduler.core.log.IJobFileAppender;
import com.quanshi.scheduler.core.thread.JobThread;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午11:34:53
 */
public class ExecutorBizImpl implements ExecutorBiz {

    private static Logger logger = LoggerFactory.getLogger(ExecutorBizImpl.class);

    @Override
    public Ret<String> beat() {
        return Ret.SUCCESS;
    }

    @Override
    public Ret<String> idleBeat(long jobId) {
        // isRunningOrHasQueue
        boolean isRunningOrHasQueue = false;
        JobThread jobThread = IJobExecutor.loadJobThread(jobId);
        if (jobThread != null && jobThread.isRunningOrHasQueue()) {
            isRunningOrHasQueue = true;
        }

        if (isRunningOrHasQueue) {
            return new Ret<String>(Ret.FAIL_CODE, "job thread is running or has trigger queue.");
        }
        return Ret.SUCCESS;
    }

    @Override
    public Ret<String> kill(long jobId) {
        // kill handlerThread, and create new one
        JobThread jobThread = IJobExecutor.loadJobThread(jobId);
        if (jobThread != null) {
            IJobExecutor.removeJobThread(jobId, "人工手动终止");
            return Ret.SUCCESS;
        }

        return new Ret<String>(Ret.SUCCESS_CODE, "job thread aleady killed.");
    }

    @Override
    public Ret<LogResult> log(long logDateTim, long logId, int fromLineNum) {
        // log filename: yyyy-MM-dd/9999.log
        String logFileName = IJobFileAppender.makeLogFileName(new Date(logDateTim), logId);

        LogResult logResult = IJobFileAppender.readLog(logFileName, fromLineNum);
        return new Ret<LogResult>(logResult);
    }

    @Override
    public Ret<String> run(TriggerParam triggerParam) {
        JobThread jobThread = IJobExecutor.loadJobThread(triggerParam.getJobId());
        IJobHandler jobHandler = jobThread != null ? jobThread.getHandler() : null;
        String removeOldReason = null;

        // valid：jobHandler + jobThread
        if (GlueTypeEnum.BEAN == GlueTypeEnum.match(triggerParam.getGlueType())) {
            // valid old jobThread
            if (jobThread != null && jobHandler != null && jobThread.getHandler() != jobHandler) {
                // change handler, need kill old thread
                removeOldReason = "更新JobHandler或更换任务模式,终止旧任务线程";

                jobThread = null;
                jobHandler = null;
            }

            // valid handler
            if (jobHandler == null) {
                jobHandler = IJobExecutor.loadJobHandler(triggerParam.getExecutorHandler());
                if (jobHandler == null) {
                    return new Ret<String>(Ret.FAIL_CODE, "job handler [" + triggerParam.getExecutorHandler() + "] not found.");
                }
            }

        } else if (GlueTypeEnum.GLUE_GROOVY == GlueTypeEnum.match(triggerParam.getGlueType())) {
            // valid old jobThread
            if (jobThread != null
                && !(jobThread.getHandler() instanceof GlueJobHandler && ((GlueJobHandler) jobThread.getHandler()).getGlueUpdatetime() == triggerParam.getGlueUpdatetime())) {
                // change handler or gluesource updated, need kill old thread
                removeOldReason = "更新任务逻辑或更换任务模式,终止旧任务线程";

                jobThread = null;
                jobHandler = null;
            }

            // valid handler
            if (jobHandler == null) {
                try {
                    IJobHandler originJobHandler = GlueFactory.getInstance().loadNewInstance(triggerParam.getGlueSource());
                    jobHandler = new GlueJobHandler(originJobHandler, triggerParam.getGlueUpdatetime());
                } catch (Exception e) {
                    logger.error("", e);
                    return new Ret<String>(Ret.FAIL_CODE, e.getMessage());
                }
            }
        } else if (GlueTypeEnum.GLUE_SHELL == GlueTypeEnum.match(triggerParam.getGlueType()) || GlueTypeEnum.GLUE_PYTHON == GlueTypeEnum.match(triggerParam.getGlueType())) {
            // valid old jobThread
            if (jobThread != null
                && !(jobThread.getHandler() instanceof ScriptJobHandler && ((ScriptJobHandler) jobThread.getHandler()).getGlueUpdatetime() == triggerParam.getGlueUpdatetime())) {
                // change script or gluesource updated, need kill old thread
                removeOldReason = "更新任务逻辑或更换任务模式,终止旧任务线程";

                jobThread = null;
                jobHandler = null;
            }

            // valid handler
            if (jobHandler == null) {
                jobHandler = new ScriptJobHandler(triggerParam.getJobId(), triggerParam.getGlueUpdatetime(), triggerParam.getGlueSource(),
                    GlueTypeEnum.match(triggerParam.getGlueType()));
            }
        } else {
            return new Ret<String>(Ret.FAIL_CODE, "glueType[" + triggerParam.getGlueType() + "] is not valid.");
        }

        // executor block strategy
        if (jobThread != null) {
            ExecutorBlockStrategyEnum blockStrategy = ExecutorBlockStrategyEnum.match(triggerParam.getExecutorBlockStrategy(), null);
            if (ExecutorBlockStrategyEnum.DISCARD_LATER == blockStrategy) {
                // discard when running
                if (jobThread.isRunningOrHasQueue()) {
                    return new Ret<String>(Ret.FAIL_CODE, "阻塞处理策略-生效：" + ExecutorBlockStrategyEnum.DISCARD_LATER.getTitle());
                }
            } else if (ExecutorBlockStrategyEnum.COVER_EARLY == blockStrategy) {
                // kill running jobThread
                if (jobThread.isRunningOrHasQueue()) {
                    removeOldReason = "阻塞处理策略-生效：" + ExecutorBlockStrategyEnum.COVER_EARLY.getTitle();

                    jobThread = null;
                }
            } else {
                // just queue trigger
            }
        }

        // replace thread (new or exists invalid)
        if (jobThread == null) {
            jobThread = IJobExecutor.registJobThread(triggerParam.getJobId(), jobHandler, removeOldReason);
        }

        // push data to queue
        Ret<String> pushResult = jobThread.pushTriggerQueue(triggerParam);
        return pushResult;
    }

}
