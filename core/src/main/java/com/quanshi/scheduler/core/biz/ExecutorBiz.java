/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.biz;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.biz.model.LogResult;
import com.quanshi.scheduler.core.biz.model.TriggerParam;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午10:23:41
 */
public interface ExecutorBiz {

    /**
     * beat
     * @return
     */
    Ret<String> beat();

    /**
     * idle beat
     *
     * @param jobId
     * @return
     */
    Ret<String> idleBeat(long jobId);

    /**
     * kill
     * @param jobId
     * @return
     */
    Ret<String> kill(long jobId);

    /**
     * log
     * @param logDateTim
     * @param logId
     * @param fromLineNum
     * @return
     */
    Ret<LogResult> log(long logDateTim, long logId, int fromLineNum);

    /**
     * run
     * @param triggerParam
     * @return
     */
    Ret<String> run(TriggerParam triggerParam);

}
