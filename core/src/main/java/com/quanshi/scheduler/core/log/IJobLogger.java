/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.log;

import java.text.MessageFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanshi.scheduler.core.utils.DateHelper;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午10:42:04
 */
public class IJobLogger {

    private static Logger logger = LoggerFactory.getLogger(IJobLogger.class);

    /**
     * append log
     *
     * @param appendLog
     */
    public static void log(String appendLog) {
        // logFileName
        String logFileName = IJobFileAppender.contextHolder.get();
        if (logFileName == null || logFileName.trim().length() == 0) {
            return;
        }

        // "yyyy-MM-dd HH:mm:ss [ClassName]-[MethodName]-[LineNumber]-[ThreadName] log";
        StackTraceElement[] stackTraceElements = new Throwable().getStackTrace();
        StackTraceElement callInfo = stackTraceElements[1];

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(DateHelper.format(new Date())).append(" ").append("[" + callInfo.getClassName() + "]").append("-").append("[" + callInfo.getMethodName() + "]")
            .append("-").append("[" + callInfo.getLineNumber() + "]").append("-").append("[" + Thread.currentThread().getName() + "]").append(" ")
            .append(appendLog != null ? appendLog : "");
        String formatAppendLog = stringBuffer.toString();

        // appendlog
        IJobFileAppender.appendLog(logFileName, formatAppendLog);

        logger.info("[{}]: {}", logFileName, formatAppendLog);
    }

    /**
     * append log with pattern
     *
     * @
     *
     * @param appendLogPattern  like "aaa {0} bbb {1} ccc"
     * @param appendLogArguments    like "111, true"
     */
    public static void log(String appendLogPattern, Object... appendLogArguments) {
        String appendLog = MessageFormat.format(appendLogPattern, appendLogArguments);
        log(appendLog);
    }
}
