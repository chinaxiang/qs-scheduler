/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.enums;

/**
 * 阻塞处理策略<br/>
 * 调度过于密集执行器来不及处理时的处理策略
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午7:49:26
 */
public enum ExecutorBlockStrategyEnum {

                                       /** 单机串行 */
                                       SERIAL_EXECUTION("单机串行"),
                                       /** 丢弃后续调度 */
                                       DISCARD_LATER("丢弃后续调度"),
                                       /** 覆盖之前调度 */
                                       COVER_EARLY("覆盖之前调度");

    /**  */
    private String title;

    /**
     * @param title
     */
    private ExecutorBlockStrategyEnum(String title) {
        this.title = title;
    }

    /**
     * match
     * 
     * @param name
     * @param defaultItem
     * @return
     */
    public static ExecutorBlockStrategyEnum match(String name, ExecutorBlockStrategyEnum defaultItem) {
        if (name != null) {
            for (ExecutorBlockStrategyEnum item : ExecutorBlockStrategyEnum.values()) {
                if (item.name().equals(name)) {
                    return item;
                }
            }
        }
        return defaultItem;
    }

    /**
     * Getter method for property <tt>title</tt>.
     * 
     * @return property value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method for property <tt>title</tt>.
     * 
     * @param title value to be assigned to property title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
