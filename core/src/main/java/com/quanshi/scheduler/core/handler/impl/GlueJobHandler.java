/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.handler.impl;

import com.quanshi.scheduler.core.base.Ret;
import com.quanshi.scheduler.core.handler.IJobHandler;
import com.quanshi.scheduler.core.log.IJobLogger;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午11:43:43
 */
public class GlueJobHandler implements IJobHandler {

    private long        glueUpdatetime;
    private IJobHandler jobHandler;

    public GlueJobHandler(IJobHandler jobHandler, long glueUpdatetime) {
        this.jobHandler = jobHandler;
        this.glueUpdatetime = glueUpdatetime;
    }

    public long getGlueUpdatetime() {
        return glueUpdatetime;
    }

    @Override
    public Ret<String> execute(String... params) throws Exception {
        IJobLogger.log("----------- glue.version:" + glueUpdatetime + " -----------");
        return jobHandler.execute(params);
    }

}
