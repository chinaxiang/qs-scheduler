/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.rpc.codec;

import java.io.Serializable;

/**
 * Rpc Response
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午9:50:59
 */
public class RpcResponse implements Serializable {

    /**  */
    private static final long serialVersionUID = 8618348590638803188L;

    private String            error;
    private Object            result;

    public boolean isError() {
        return error != null;
    }

    /**
     * Getter method for property <tt>error</tt>.
     * 
     * @return property value of error
     */
    public String getError() {
        return error;
    }

    /**
     * Setter method for property <tt>error</tt>.
     * 
     * @param error value to be assigned to property error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Getter method for property <tt>result</tt>.
     * 
     * @return property value of result
     */
    public Object getResult() {
        return result;
    }

    /**
     * Setter method for property <tt>result</tt>.
     * 
     * @param result value to be assigned to property result
     */
    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "NettyResponse {error=" + error + ", result=" + result + "}";
    }

}
