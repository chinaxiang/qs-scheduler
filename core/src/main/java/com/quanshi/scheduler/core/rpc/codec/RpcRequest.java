/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.rpc.codec;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Rpc Request
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午9:49:06
 */
public class RpcRequest implements Serializable {

    /**  */
    private static final long serialVersionUID = 4741979274714311522L;

    private String            serverAddress;
    private long              createMillisTime;

    private String            className;
    private String            methodName;
    private Class<?>[]        parameterTypes;
    private Object[]          parameters;

    /**
     * Getter method for property <tt>serverAddress</tt>.
     * 
     * @return property value of serverAddress
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * Setter method for property <tt>serverAddress</tt>.
     * 
     * @param serverAddress value to be assigned to property serverAddress
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * Getter method for property <tt>createMillisTime</tt>.
     * 
     * @return property value of createMillisTime
     */
    public long getCreateMillisTime() {
        return createMillisTime;
    }

    /**
     * Setter method for property <tt>createMillisTime</tt>.
     * 
     * @param createMillisTime value to be assigned to property createMillisTime
     */
    public void setCreateMillisTime(long createMillisTime) {
        this.createMillisTime = createMillisTime;
    }

    /**
     * Getter method for property <tt>className</tt>.
     * 
     * @return property value of className
     */
    public String getClassName() {
        return className;
    }

    /**
     * Setter method for property <tt>className</tt>.
     * 
     * @param className value to be assigned to property className
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * Getter method for property <tt>methodName</tt>.
     * 
     * @return property value of methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Setter method for property <tt>methodName</tt>.
     * 
     * @param methodName value to be assigned to property methodName
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Getter method for property <tt>parameterTypes</tt>.
     * 
     * @return property value of parameterTypes
     */
    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    /**
     * Setter method for property <tt>parameterTypes</tt>.
     * 
     * @param parameterTypes value to be assigned to property parameterTypes
     */
    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    /**
     * Getter method for property <tt>parameters</tt>.
     * 
     * @return property value of parameters
     */
    public Object[] getParameters() {
        return parameters;
    }

    /**
     * Setter method for property <tt>parameters</tt>.
     * 
     * @param parameters value to be assigned to property parameters
     */
    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "NettyRequest{serverAddress=" + serverAddress + 
                ", createMillisTime=" + createMillisTime + 
                ", className=" + className + 
                ", methodName=" + methodName + 
                ", parameterTypes=" + Arrays.toString(parameterTypes) + 
                ", parameters=" + Arrays.toString(parameters) + 
                "}";
    }

}
