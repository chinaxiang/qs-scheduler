/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.rpc.netcom.jetty.server;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.thread.ExecutorThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanshi.scheduler.core.thread.ExecutorRegistryThread;

/**
 * 内置jetty服务，提供非阻塞通信支持
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午11:25:23
 */
public class JettyServer {

    private static final Logger logger = LoggerFactory.getLogger(JettyServer.class);

    private Server              server;
    private Thread              thread;

    public void start(final int port, final String ip, final String appName) throws Exception {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {

                // The Server
                server = new Server(new ExecutorThreadPool()); // 非阻塞

                // HTTP connector
                ServerConnector connector = new ServerConnector(server);
                connector.setPort(port);
                server.setConnectors(new Connector[] { connector });

                // Set a handler
                HandlerCollection handlerc = new HandlerCollection();
                handlerc.setHandlers(new Handler[] { new JettyServerHandler() });
                server.setHandler(handlerc);

                try {
                    // Start the server
                    server.start();
                    logger.info(">>>>>>>>>>>> job jetty server start success at port:{}.", port);
                    ExecutorRegistryThread.getInstance().start(port, ip, appName);
                    server.join(); // block until thread stopped
                    logger.info(">>>>>>>>>>> rpc server join success, netcon={}, port={}", JettyServer.class.getName(), port);
                } catch (Exception e) {
                    logger.error("", e);
                } finally {
                    destroy();
                }
            }
        });
        thread.setDaemon(true); // daemon, service jvm, user thread leave >>> daemon leave >>> jvm leave
        thread.start();
    }

    public void destroy() {
        if (server != null) {
            try {
                server.stop();
                server.destroy();
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        if (thread.isAlive()) {
            thread.interrupt();
        }
        logger.info(">>>>>>>>>>> rpc server destroy success, netcon={}", JettyServer.class.getName());
    }
}
