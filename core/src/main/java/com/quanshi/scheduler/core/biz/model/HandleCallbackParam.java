/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.biz.model;

import java.io.Serializable;

import com.quanshi.scheduler.core.base.Ret;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午10:32:38
 */
public class HandleCallbackParam implements Serializable {

    /**  */
    private static final long serialVersionUID = 4540301676333767056L;

    private long               logId;
    private Ret<String>   executeResult;

    public HandleCallbackParam() {
    }

    public HandleCallbackParam(long logId, Ret<String> executeResult) {
        this.logId = logId;
        this.executeResult = executeResult;
    }

    /**
     * Getter method for property <tt>logId</tt>.
     * 
     * @return property value of logId
     */
    public long getLogId() {
        return logId;
    }

    /**
     * Setter method for property <tt>logId</tt>.
     * 
     * @param logId value to be assigned to property logId
     */
    public void setLogId(long logId) {
        this.logId = logId;
    }

    /**
     * Getter method for property <tt>executeResult</tt>.
     * 
     * @return property value of executeResult
     */
    public Ret<String> getExecuteResult() {
        return executeResult;
    }

    /**
     * Setter method for property <tt>executeResult</tt>.
     * 
     * @param executeResult value to be assigned to property executeResult
     */
    public void setExecuteResult(Ret<String> executeResult) {
        this.executeResult = executeResult;
    }
    
    @Override
    public String toString() {
        return "HandleCallbackParam{" +
                "logId=" + logId +
                ", executeResult=" + executeResult +
                "}";
    }

}
