/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

/**
 * 日期帮助类
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午7:58:56
 */
public class DateHelper {

    /** yyyy-MM-dd */
    public static String datePattern     = "yyyy-MM-dd";
    /** yyyy-MM-dd HH:mm:ss */
    public static String dateTimePattern = "yyyy-MM-dd HH:mm:ss";
    /** yyyy-MM-dd */
    public static SimpleDateFormat dateFormat     = new SimpleDateFormat(datePattern);
    /** yyyy-MM-dd HH:mm:ss */
    public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat(dateTimePattern);
    
    /**
     * yyyy-MM-dd HH:mm:ss
     * 
     * @param date
     * @return
     */
    public static String format(Date date) {
        return dateTimeFormat.format(date);
    }

    /**
     * yyyy-MM-dd HH:mm:ss
     *
     * @param dateStr
     * @return
     */
    public static Date parse( String dateStr )
    {
        try
        {
            return DateUtils.parseDate( dateStr, dateTimeFormat.toPattern() );
        }
        catch ( ParseException e )
        {
            return null;
        }
    }

}
