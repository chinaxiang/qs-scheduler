/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.base;

import java.io.Serializable;

/**
 * 响应体
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午8:27:14
 */
public class Ret<T> implements Serializable {

    /**  */
    private static final long           serialVersionUID = 1713144909811441472L;

    public static final int             SUCCESS_CODE     = 200;
    public static final int             FAIL_CODE        = 500;
    public static final Ret<String> SUCCESS          = new Ret<String>(null);
    public static final Ret<String> FAIL             = new Ret<String>(FAIL_CODE, null);

    private int                         code;
    private String                      msg;
    private T                           content;

    public Ret() {
    }

    public Ret(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Ret(T content) {
        this.code = SUCCESS_CODE;
        this.content = content;
    }

    /**
     * Getter method for property <tt>code</tt>.
     * 
     * @return property value of code
     */
    public int getCode() {
        return code;
    }

    /**
     * Setter method for property <tt>code</tt>.
     * 
     * @param code value to be assigned to property code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Getter method for property <tt>msg</tt>.
     * 
     * @return property value of msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Setter method for property <tt>msg</tt>.
     * 
     * @param msg value to be assigned to property msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * Getter method for property <tt>content</tt>.
     * 
     * @return property value of content
     */
    public T getContent() {
        return content;
    }

    /**
     * Setter method for property <tt>content</tt>.
     * 
     * @param content value to be assigned to property content
     */
    public void setContent(T content) {
        this.content = content;
    }
    
    @Override
    public String toString() {
        return "Ret{" +
                "code=" + code +
                ", msg=" + msg +
                ", content=" + content +
                "}";
    }

}
