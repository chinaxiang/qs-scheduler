/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.handler;

import com.quanshi.scheduler.core.base.Ret;

/**
 * Job Handler接口
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午8:36:18
 */
public interface IJobHandler {

    /**
     * execute job
     * 
     * @param params
     * @return
     * @throws Exception
     */
    Ret<String> execute(String... params) throws Exception;
}
