/**
 * QUANSHI.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.quanshi.scheduler.core.base;

/**
 * 
 * 
 * @author chinaxiang
 * @version 2017年7月7日 下午11:29:52
 */
public class RegistryConfig {

    public static final int BEAT_TIMEOUT = 30;
    public static final int DEAD_TIMEOUT = BEAT_TIMEOUT * 3;

    public enum RegistType {
                            EXECUTOR, ADMIN
    }

}
