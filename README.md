# qs-scheduler

分布式云调度处理系统。

项目基于quartz并进行若干扩展而成，适用于公司内部做定时调度处理，方便，快捷，简单。

支持bean, groovy, shell, python四种任务处理方式。

项目架构图

![](http://qcdn.37ads.com/blog/screenshot_20170727175625.png/blog)

- core: 云调度核心，封装了各种工具，基础组件。
- job: quartz可视化控制台及quartz调度器，去调度具体的executor, 内置了一个最简单的executor.
- executor: 可选处理器，可扩展，可自定义，可分布式部署。

技术选型

![](http://qcdn.37ads.com/blog/screenshot_20170728095458.png/blog)

业务架构图

![](http://qcdn.37ads.com/blog/screenshot_20170728095706.png/blog)

job可视化控制台架构图

![](http://qcdn.37ads.com/blog/screenshot_20170728100005.png/blog)

控制台首页

![](http://qcdn.37ads.com/blog/screenshot_20170728100155.png/blog)

任务管理

![](http://qcdn.37ads.com/blog/screenshot_20170728100316.png/blog)

脚本编辑器

![](http://qcdn.37ads.com/blog/screenshot_20170728100646.png/blog)

日志管理

![](http://qcdn.37ads.com/blog/screenshot_20170728100418.png/blog)

日志控制台

![](http://qcdn.37ads.com/blog/screenshot_20170728100757.png/blog)

集群管理

![](http://qcdn.37ads.com/blog/screenshot_20170728100512.png/blog)

## 项目配置

项目采用分模块开发，其中的微服务模块：facade, facade-impl, integration为测试模块，没有使用，可以自行去掉。

项目下载下来，只需要更改日志路径及数据库相关配置即可正常使用。

日志配置在：conf/config/logback.xml

数据库配置在：conf/config/application.properties

执行器模块比较简单，主要用来开发一些自定义的bean处理类。默认job模块中已经内置了一个基础处理器。

数据库表定义在：dal 模块中的 qs_scheduler.sql 中，创建所需要的表即可。其中11张表是quartz的表，另外5张表是业务扩展表。

## 项目站点

项目集成了一些maven的插件，如findbugs, taglist, 你可以有选择的生成项目站点，查看项目报告。

```
mvn clean site
```

即可在target目录下得到 site 信息。

![](http://qcdn.37ads.com/blog/screenshot_20170728104254.png/blog)

各个子模块的target目录下都有 site 信息，可以查看项目报告，如findbugs。

![](http://qcdn.37ads.com/blog/screenshot_20170728104654.png/blog)

## 项目启动

数据库和配置修改完毕后，直接在项目目录执行：

```
mvn clean package -DskipTests
```

将项目 `job/target/job.war` 拷贝到 tomcat 的webapps下，启动tomcat即可。


